package progettomobile.balancemanager.utils;

import androidx.annotation.NonNull;

import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class BalanceUtils {

    private BalanceUtils() {}

    public static int parseBalanceValue(@NonNull String stringBalance) {
        String[] splitBalance = stringBalance.split("[\\,\\.]");
        int integralValue = 0;
        if (!splitBalance[0].isEmpty() && !splitBalance[0].equals("-")) {
            integralValue = Integer.parseInt(splitBalance[0]) * 100;
        }
        int centValue = 0;
        if (splitBalance.length > 1 && !splitBalance[1].isEmpty()) {
            int signum = Integer.signum(integralValue);
            if (signum == 0) {
                if (splitBalance[0].contains("-")) {
                    signum = -1;
                } else {
                    signum = 1;
                }
            }
            centValue = Integer.parseInt(adjustLengthToRight(splitBalance[1])) * signum;
        }
        return integralValue + centValue;
    }

    public static String balanceStringFromInt(int balanceValue) {
        return  (balanceValue < 0 ? "-" : "")
                + String.valueOf(Math.abs(Math.round(balanceValue >= 0 ? Math.floor(balanceValue / 100.0) : Math.ceil(balanceValue / 100.0))))
                + DecimalFormatSymbols.getInstance(Locale.getDefault()).getDecimalSeparator()
                + adjustLengthToLeft(String.valueOf(Math.abs(balanceValue) % 100));
    }

    private static String adjustLengthToRight(String number) {
        if (number.length() > 2) {
            return number.substring(0, 2);
        } else if (number.length() == 1) {
            return number + "0";
        } else {
            return number;
        }
    }

    private static String adjustLengthToLeft(String number) {
        if (number.length() > 2) {
            return number.substring(0, 2);
        } else if (number.length() == 1) {
            return "0" + number;
        } else {
            return number;
        }
    }
}
