package progettomobile.balancemanager.utils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import progettomobile.balancemanager.R;

public class FragmentManagementUtils {
    private FragmentManagementUtils() {}

    public static void replaceFragment(AppCompatActivity activity, Fragment fragment, String tag) {
        FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.object_insertion_fragment, fragment, tag);
        fragmentTransaction.commit();
    }
}
