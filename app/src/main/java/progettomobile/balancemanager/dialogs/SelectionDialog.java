package progettomobile.balancemanager.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.widget.RadioButton;

import androidx.annotation.NonNull;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.activities.BackableActivity;
import progettomobile.balancemanager.calendarview.views.CalendarView;
import progettomobile.balancemanager.constants.IntentKeys;
import progettomobile.balancemanager.constants.ManagedEntity;
import progettomobile.balancemanager.constants.RequestCodes;
import progettomobile.balancemanager.database.Database;

import org.joda.time.LocalDate;

public class SelectionDialog extends Dialog {

    private int selectedRadioButton = 0;

    public SelectionDialog(@NonNull Activity activity) {
        super(activity);
        setContentView(R.layout.add_operation_dialog);

        ((RadioButton) findViewById(R.id.transaction_radioButton)).setChecked(true);
        findViewById(R.id.transaction_radioButton).setOnClickListener(v -> selectedRadioButton = 0);
        if (Database.getAccountList().size() < 2) {
            findViewById(R.id.transfer_radioButton).setEnabled(false);
        }
        findViewById(R.id.transfer_radioButton).setOnClickListener(v -> selectedRadioButton = 1);
        findViewById(R.id.cancel_button).setOnClickListener(v -> dismiss());
        findViewById(R.id.ok_button).setOnClickListener(v -> {
            Intent intent = new Intent(activity, BackableActivity.class);
            if (selectedRadioButton == 1) {
                intent.putExtra(IntentKeys.MANAGED_ENTITY, ManagedEntity.TRANSFER);
            } else {
                intent.putExtra(IntentKeys.MANAGED_ENTITY, ManagedEntity.TRANSACTION);
            }
            LocalDate date;
            CalendarView calendarView = activity.findViewById(R.id.history_calendarview);
            if (calendarView == null) {
                date = LocalDate.now();
            } else {
                date = calendarView.getCalendarViewDelegate().getSelectedDate().toJodaDate();
            }
            intent.putExtra(IntentKeys.TRANSACTION_DATE, date.toString());
            activity.startActivityForResult(intent, RequestCodes.REQUEST_NEW_TRANSACTION);
            dismiss();
        });

    }

}
