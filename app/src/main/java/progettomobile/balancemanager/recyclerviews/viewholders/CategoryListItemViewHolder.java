package progettomobile.balancemanager.recyclerviews.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import progettomobile.balancemanager.R;

public class CategoryListItemViewHolder extends RecyclerView.ViewHolder {

    private TextView titleTextView;
    private ImageView editIconImageView;

    public CategoryListItemViewHolder(@NonNull View itemView) {
        super(itemView);
        titleTextView = itemView.findViewById(R.id.category_item_textView);
        editIconImageView = itemView.findViewById(R.id.edit_icon_imageView);
    }

    public void setText(String text) {
        this.titleTextView.setText(text);
    }

    public void setEditIconVisibility (boolean value) {
        editIconImageView.setVisibility(value ? View.VISIBLE : View.GONE);
    }

    public void setEditIconClickListener(View.OnClickListener listener) {
        editIconImageView.setOnClickListener(listener);
    }

}
