package progettomobile.balancemanager.recyclerviews.viewholders;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.constants.SharedPreferencesKeys;
import progettomobile.balancemanager.utils.BalanceUtils;

public class TransactionListItemViewHolder extends RecyclerView.ViewHolder {

    private TextView transactionDescription;
    private TextView transactionValue;
    private TextView transactionCategory;
    private ImageView editIcon;

    public TransactionListItemViewHolder(@NonNull View itemView) {
        super(itemView);
        transactionDescription = itemView.findViewById(R.id.transaction_desc_textView);
        transactionValue = itemView.findViewById(R.id.transaction_value_textView);
        editIcon = itemView.findViewById(R.id.transaction_editIcon);
        transactionCategory = itemView.findViewById(R.id.transaction_category_textView);
    }

    public void setTransactionDescription(String text) {
        transactionDescription.setText(text);
    }

    public void setTransactionCategory(String text) {
        transactionCategory.setText(text);
    }

    public void setTransactionValue(int value) {
        final Context context = transactionValue.getContext();
        final String displayedPlus = value > 0 ? "+" : "";
        final String currencySymbol = context.getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE).getString(SharedPreferencesKeys.CURRENCY_PREFERENCE, "€");
        transactionValue.setText(context.getString(R.string.balance_with_currency, displayedPlus + BalanceUtils.balanceStringFromInt(value), currencySymbol));
        if (value > 0) {
            transactionValue.setBackgroundColor(transactionValue.getResources().getColor(R.color.balanceTextBackgroundPositive));
        } else if (value < 0) {
            transactionValue.setBackgroundColor(transactionValue.getResources().getColor(R.color.balanceTextBackgroundNegative));
        } else {
            transactionValue.setBackgroundColor(transactionValue.getResources().getColor(R.color.balanceTextBackgroundNeutral));
        }
    }

    public void setEditIconListener(View.OnClickListener listener) {
        editIcon.setOnClickListener(listener);
    }

    public void setEditIconVisibility(boolean value) {
        editIcon.setVisibility(value ? View.VISIBLE : View.GONE);
    }

}
