package progettomobile.balancemanager.recyclerviews.viewholders;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.recyclerview.widget.RecyclerView;

import progettomobile.balancemanager.R;

public class SettingSectionItemViewHolder extends RecyclerView.ViewHolder {

    private TextView nameTextView;
    private View itemView;

    public SettingSectionItemViewHolder(@NonNull View itemView) {
        super(itemView);
        nameTextView = itemView.findViewById(R.id.setting_section_name_textView);
        this.itemView = itemView;
    }

    public void setSettingName(@NonNull String name) {
        nameTextView.setText(name);
    }

    public void setSettingName(@StringRes int stringId) {
        nameTextView.setText(stringId);
    }

    public void setItemViewOnClickListener(View.OnClickListener listener) {
        itemView.setOnClickListener(listener);
    }
}
