package progettomobile.balancemanager.recyclerviews.viewholders;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.TextView;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.recyclerviews.OnItemClickListener;

public class LateralMenuItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView pageTitleTextView;
    private OnItemClickListener onItemClickListener;

    public LateralMenuItemViewHolder(@NonNull View itemView, OnItemClickListener onItemClickListener) {
        super(itemView);
        pageTitleTextView = itemView.findViewById(R.id.lateral_menu_item_title_textView);
        this.onItemClickListener = onItemClickListener;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        onItemClickListener.onItemClick(getAdapterPosition());
    }

    public void setText(String text) {
        if (pageTitleTextView != null) {
            pageTitleTextView.setText(text);
        }
    }
}
