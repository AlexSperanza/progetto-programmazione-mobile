package progettomobile.balancemanager.recyclerviews.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.constants.ApplicationPage;
import progettomobile.balancemanager.database.Database;
import progettomobile.balancemanager.database.entities.Account;
import progettomobile.balancemanager.recyclerviews.OnItemClickListener;
import progettomobile.balancemanager.recyclerviews.viewholders.LateralMenuItemViewHolder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LateralMenuItemAdapter extends RecyclerView.Adapter<LateralMenuItemViewHolder> {

    //Lista pagine
    private List<ApplicationPage> applicationPageList = Arrays.asList(ApplicationPage.values());
    private OnItemClickListener onItemClickListener;
    private ViewGroup parent;

    public LateralMenuItemAdapter(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public LateralMenuItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.lateral_menu_item, parent, false);
        this.parent = parent;
        return new LateralMenuItemViewHolder(itemView, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull LateralMenuItemViewHolder holder, int position) {
        //Prendi valori da lista e setta i vari campi dell'holder
        if (parent != null) {
            if (position == 0) {
                final Account lastAccount = Database.getCurrentAccount();
                if (lastAccount != null) {
                    holder.setText(parent.getResources().getString(R.string.account_name_summary, lastAccount.getAccountName()));
                } else { //If current account is not found (should not be possible)
                    holder.setText(parent.getResources().getString(applicationPageList.get(position).stringId));
                }
            } else {
                holder.setText(parent.getResources().getString(applicationPageList.get(position).stringId));
            }
        }
    }

    @Override
    public int getItemCount() {
        return applicationPageList.size();
    }

    public List<ApplicationPage> getPageList() {
        return new ArrayList<>(applicationPageList);
    }
}
