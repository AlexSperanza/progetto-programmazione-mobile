package progettomobile.balancemanager.recyclerviews.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.activities.BackableActivity;
import progettomobile.balancemanager.constants.IntentKeys;
import progettomobile.balancemanager.constants.ManagedEntity;
import progettomobile.balancemanager.constants.RequestCodes;
import progettomobile.balancemanager.database.entities.Category;
import progettomobile.balancemanager.recyclerviews.viewholders.CategoryListItemViewHolder;

import java.util.List;

public class CategoryListItemAdapter extends RecyclerView.Adapter<CategoryListItemViewHolder> {

    private List<Category> categoryList;
    private ViewGroup parent;
    private Activity activity;

    public CategoryListItemAdapter(List<Category> categoryList, Activity activity) {
        this.categoryList = categoryList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public CategoryListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_list_item, parent, false);
        if (this.parent == null) {
            this.parent = parent;
        }
        return new CategoryListItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryListItemViewHolder holder, int position) {
        if (parent != null) {
            String text = categoryList.get(position).isDefault()
                            ? parent.getResources().getString(Category.DEFAULT_STRING_MAP.get(categoryList.get(position).getCategoryName()))
                            : categoryList.get(position).getCategoryName();
            boolean visibility = !categoryList.get(position).isDefault();
            holder.setText(text);
            holder.setEditIconVisibility(visibility);
            holder.setEditIconClickListener(v -> {
                if (activity != null) {
                    Intent intent = new Intent(v.getContext(), BackableActivity.class);
                    intent.putExtra(IntentKeys.EDIT_CATEGORY_ID, categoryList.get(position).getIdCategory());
                    intent.putExtra(IntentKeys.CALLING_ACTIVITY, BackableActivity.class.getName());
                    intent.putExtra(IntentKeys.MANAGED_ENTITY, ManagedEntity.CATEGORY);
                    activity.startActivityForResult(intent, RequestCodes.REQUEST_EDIT_CATEGORY);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }
}
