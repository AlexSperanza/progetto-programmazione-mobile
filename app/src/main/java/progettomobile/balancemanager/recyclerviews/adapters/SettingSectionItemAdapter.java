package progettomobile.balancemanager.recyclerviews.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.activities.BackableActivity;
import progettomobile.balancemanager.constants.IntentKeys;
import progettomobile.balancemanager.constants.ManagedEntity;
import progettomobile.balancemanager.constants.RequestCodes;
import progettomobile.balancemanager.database.entities.Account;
import progettomobile.balancemanager.recyclerviews.viewholders.SettingSectionItemViewHolder;

import java.util.List;

public class SettingSectionItemAdapter extends RecyclerView.Adapter<SettingSectionItemViewHolder> {

    private List<Account> accountList;
    private Activity activity;

    public SettingSectionItemAdapter(List<Account> accountList, @NonNull Activity activity) {
        this.accountList = accountList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public SettingSectionItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.setting_section_list_item, parent, false);
        return new SettingSectionItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SettingSectionItemViewHolder holder, int position) {
        if (position == 0) { //General settings
            holder.setSettingName(R.string.general_settings);
            holder.setItemViewOnClickListener(v -> {
                Intent intent = new Intent(activity, BackableActivity.class);
                intent.putExtra(IntentKeys.MANAGED_ENTITY, ManagedEntity.SETTING_PAGE);
                intent.putExtra(IntentKeys.SETTING_ACCOUNT_ID, IntentKeys.GENERAL_SETTINGS);
                activity.startActivityForResult(intent, RequestCodes.REQUEST_SETTINGS);
            });
        } else { // Account-specific settings
            holder.setSettingName(accountList.get(position).getAccountName());
            holder.setItemViewOnClickListener(v -> {
                Intent intent = new Intent(activity, BackableActivity.class);
                intent.putExtra(IntentKeys.MANAGED_ENTITY, ManagedEntity.SETTING_PAGE);
                intent.putExtra(IntentKeys.SETTING_ACCOUNT_ID, accountList.get(position).getIdAccount());
                activity.startActivityForResult(intent, RequestCodes.REQUEST_SETTINGS);
            });
        }
    }

    @Override
    public int getItemCount() {
        return accountList.size();
    }
}
