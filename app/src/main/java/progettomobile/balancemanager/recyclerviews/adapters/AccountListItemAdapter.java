package progettomobile.balancemanager.recyclerviews.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.activities.BackableActivity;
import progettomobile.balancemanager.activities.HomeActivity;
import progettomobile.balancemanager.constants.ApplicationPage;
import progettomobile.balancemanager.constants.IntentKeys;
import progettomobile.balancemanager.constants.ManagedEntity;
import progettomobile.balancemanager.constants.RequestCodes;
import progettomobile.balancemanager.constants.SharedPreferencesKeys;
import progettomobile.balancemanager.database.entities.Account;
import progettomobile.balancemanager.recyclerviews.viewholders.AccountListItemViewHolder;

import java.util.List;

public class AccountListItemAdapter extends RecyclerView.Adapter<AccountListItemViewHolder> {

    private List<Account> accountList;
    private Activity activity;

    public AccountListItemAdapter(final List<Account> accountList, Activity activity) {
        this.accountList = accountList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public AccountListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.account_list_item, parent, false);
        return new AccountListItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AccountListItemViewHolder holder, int position) {
        if (activity != null) {
            holder.setAccountName(accountList.get(position).getAccountName());
            holder.setAccountBalance(accountList.get(position).getAccountBalance());
            holder.setEditListener(v -> {
                Intent intent = new Intent(v.getContext(), BackableActivity.class);
                intent.putExtra(IntentKeys.EDIT_ACCOUNT_ID, accountList.get(position).getIdAccount());
                intent.putExtra(IntentKeys.MANAGED_ENTITY, ManagedEntity.ACCOUNT);
                activity.startActivityForResult(intent, RequestCodes.REQUEST_EDIT_ACCOUNT);
            });
            holder.setTextViewAreaListener(v -> {
                if (activity != null) {
                    activity.getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE)
                            .edit()
                            .putLong(SharedPreferencesKeys.LAST_ACCOUNT, accountList.get(position).getIdAccount())
                            .apply();
                    Intent returnIntent = new Intent(activity, HomeActivity.class);
                    returnIntent.putExtra(IntentKeys.PAGE, ApplicationPage.LAST_ACCOUNT);
                    activity.startActivity(returnIntent);
                    activity.finish();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return accountList.size();
    }
}
