package progettomobile.balancemanager.recyclerviews.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.activities.BackableActivity;
import progettomobile.balancemanager.constants.IntentKeys;
import progettomobile.balancemanager.constants.ManagedEntity;
import progettomobile.balancemanager.constants.RequestCodes;
import progettomobile.balancemanager.database.Database;
import progettomobile.balancemanager.database.entities.Category;
import progettomobile.balancemanager.database.entities.Transaction;
import progettomobile.balancemanager.database.relations.TransactionWithCategory;
import progettomobile.balancemanager.recyclerviews.viewholders.TransactionListItemViewHolder;

import java.util.List;

public class TransactionListItemAdapter extends RecyclerView.Adapter<TransactionListItemViewHolder> {

    private List<TransactionWithCategory> transactionList;
    private ViewGroup parent;
    private Activity activity;

    public TransactionListItemAdapter(List<TransactionWithCategory> transactionList, Activity activity) {
        this.transactionList = transactionList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public TransactionListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_list_item, parent, false);
        if (this.parent == null) {
            this.parent = parent;
        }

        return new TransactionListItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionListItemViewHolder holder, int position) {
        if (parent != null) {
            final Transaction transaction = transactionList.get(position).getTransaction();
            final Category category = transactionList.get(position).getCategory();
            holder.setTransactionDescription(transaction.getDescription());
            holder.setTransactionValue(transaction.getCentValue());
            if (Category.DEFAULT_STRING_MAP.containsKey(category.getCategoryName())) {
                String categoryString = parent.getResources().getString(Category.DEFAULT_STRING_MAP.get(category.getCategoryName()));
                if (category.getCategoryName().equals("TRANSFER")) {
                    if (transaction.getCentValue() < 0) {   //sender
                        categoryString += parent.getResources().getString(R.string.transfer_to);
                    } else {
                        categoryString += parent.getResources().getString(R.string.transfer_from);
                        holder.setEditIconVisibility(false);
                    }
                    categoryString += Database.findAccount(transaction.getTargetAccountId()).getAccountName();
                }
                holder.setTransactionCategory(categoryString);
            } else {
                holder.setTransactionCategory(category.getCategoryName());
            }
            if (category.getIdCategory() <= 1) {
                holder.setEditIconVisibility(false);
            } else {
                holder.setEditIconListener(v -> {
                    if (activity != null) {
                        Intent intent = new Intent(activity, BackableActivity.class);
                        intent.putExtra(IntentKeys.EDIT_TRANSACTION_ID, transaction.getIdTransaction());
                        intent.putExtra(IntentKeys.TRANSACTION_DATE, transaction.getDate().toString());
                        intent.putExtra(IntentKeys.MANAGED_ENTITY, transaction.getTargetAccountId() == 0 ? ManagedEntity.TRANSACTION : ManagedEntity.TRANSFER);
                        activity.startActivityForResult(intent, RequestCodes.REQUEST_EDIT_TRANSACTION);
                    }
                });
            }

        }
    }

    @Override
    public int getItemCount() {
        return transactionList.size();
    }
}
