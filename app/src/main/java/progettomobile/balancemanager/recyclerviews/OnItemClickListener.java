package progettomobile.balancemanager.recyclerviews;

public interface OnItemClickListener {
    void onItemClick(int position);
}
