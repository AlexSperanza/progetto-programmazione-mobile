package progettomobile.balancemanager.appwidget.providers;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.widget.RemoteViews;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.constants.SharedPreferencesKeys;
import progettomobile.balancemanager.database.Database;
import progettomobile.balancemanager.database.entities.Account;
import progettomobile.balancemanager.utils.BalanceUtils;

public class SingleAccountAppWidgetProvider extends AppWidgetProvider {

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Database.initAppDatabase(context);
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            context.getSharedPreferences(SharedPreferencesKeys.APP_WIDGET_PREFIX + appWidgetId, Context.MODE_PRIVATE)
                    .edit()
                    .remove(SharedPreferencesKeys.APP_WIDGET_ACCOUNT_ID)
                    .apply();
        }
    }

    public static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        long accountId = context
                .getSharedPreferences(SharedPreferencesKeys.APP_WIDGET_PREFIX + appWidgetId, Context.MODE_PRIVATE)
                .getLong(SharedPreferencesKeys.APP_WIDGET_ACCOUNT_ID, SharedPreferencesKeys.NO_CURRENT_ACCOUNT);
        Account account = Database.findAccount(accountId);
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.single_account_app_widget_layout);
        if (account != null) {
            String account_name = account.getAccountName();
            int accountBalance = account.getAccountBalance();
            final String currencySymbol = context.getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE).getString(SharedPreferencesKeys.CURRENCY_PREFERENCE, "€");
            views.setTextViewText(R.id.app_widget_account_name_textView, account_name);
            views.setTextViewText(R.id.app_widget_account_balance_textView, context.getString(R.string.balance_with_currency, BalanceUtils.balanceStringFromInt(accountBalance), currencySymbol));
        } else {
            views.setTextViewText(R.id.app_widget_account_name_textView, context.getString(R.string.app_widget_account_not_found));
            views.setTextViewText(R.id.app_widget_account_balance_textView, "");
        }
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }
}
