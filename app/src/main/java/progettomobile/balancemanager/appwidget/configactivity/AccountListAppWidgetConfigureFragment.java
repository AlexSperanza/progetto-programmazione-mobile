package progettomobile.balancemanager.appwidget.configactivity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.database.Database;

public class AccountListAppWidgetConfigureFragment extends Fragment {

    private SingleAccountAppWidgetConfigureActivity activity;

    public AccountListAppWidgetConfigureFragment(@NonNull SingleAccountAppWidgetConfigureActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (container != null) {
            container.removeAllViews();
        }
        View view = inflater.inflate(R.layout.app_widget_config_fragment, container, false);
        setupRecyclerView(view);
        return view;
    }

    private void setupRecyclerView(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.app_widget_configure_recyclerView);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        AppWidgetAccountListItemAdapter adapter = new AppWidgetAccountListItemAdapter(Database.getAccountList(), activity);
        recyclerView.setAdapter(adapter);
    }
}
