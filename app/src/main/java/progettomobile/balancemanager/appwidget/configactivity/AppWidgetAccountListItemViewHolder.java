package progettomobile.balancemanager.appwidget.configactivity;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.constants.SharedPreferencesKeys;
import progettomobile.balancemanager.utils.BalanceUtils;

public class AppWidgetAccountListItemViewHolder extends RecyclerView.ViewHolder {

    private TextView accountNameTextView;
    private TextView accountBalanceTextView;
    private LinearLayout textViewArea;

    public AppWidgetAccountListItemViewHolder(@NonNull View itemView) {
        super(itemView);
        accountNameTextView = itemView.findViewById(R.id.account_name_textView);
        accountBalanceTextView = itemView.findViewById(R.id.account_balance_textView);
        textViewArea = itemView.findViewById(R.id.account_item_text_container);
    }

    public void setAccountName(String name) {
        accountNameTextView.setText(name);
    }

    public void setAccountBalance(int balance) {
        final Context context = accountBalanceTextView.getContext();
        final String currencySymbol = context.getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE).getString(SharedPreferencesKeys.CURRENCY_PREFERENCE, "€");
        accountBalanceTextView.setText(context.getString(R.string.balance_with_currency, BalanceUtils.balanceStringFromInt(balance), currencySymbol));
        if (balance > 0) {
            accountBalanceTextView.setBackgroundColor(accountBalanceTextView.getResources().getColor(R.color.balanceTextBackgroundPositive));
        } else if (balance < 0) {
            accountBalanceTextView.setBackgroundColor(accountBalanceTextView.getResources().getColor(R.color.balanceTextBackgroundNegative));
        } else {
            accountBalanceTextView.setBackgroundColor(accountBalanceTextView.getResources().getColor(R.color.balanceTextBackgroundNeutral));
        }
    }


    public void setTextViewAreaListener(View.OnClickListener textViewAreaListener) {
        textViewArea.setOnClickListener(textViewAreaListener);
    }

}
