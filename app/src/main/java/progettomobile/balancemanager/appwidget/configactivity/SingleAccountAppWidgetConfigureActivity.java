package progettomobile.balancemanager.appwidget.configactivity;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.activities.DataSyncListener;
import progettomobile.balancemanager.appwidget.providers.SingleAccountAppWidgetProvider;
import progettomobile.balancemanager.constants.SharedPreferencesKeys;
import progettomobile.balancemanager.database.Database;

public class SingleAccountAppWidgetConfigureActivity extends AppCompatActivity implements DataSyncListener, AccountSelectedListener {

    @Override
    public void onDataSynced(Runnable callback) {
        Database.removeObserver(this);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.app_widget_configure_fragment_container, new AccountListAppWidgetConfigureFragment(this))
                .commit();
    }

    @Override
    public void onAccountSelected(long accountId) {
        int appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            appWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }
        if (appWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
            getSharedPreferences(SharedPreferencesKeys.APP_WIDGET_PREFIX + appWidgetId, MODE_PRIVATE)
                    .edit()
                    .putLong(SharedPreferencesKeys.APP_WIDGET_ACCOUNT_ID, accountId)
                    .apply();
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this.getApplicationContext());
            SingleAccountAppWidgetProvider.updateAppWidget(this, appWidgetManager, appWidgetId);
            Intent resultIntent = new Intent();
            resultIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            setResult(RESULT_OK, resultIntent);
            finish();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(RESULT_CANCELED);
        setContentView(R.layout.app_widget_configure_activity);
        Database.registerObserver(this, null);
        Database.initAppDatabase(this.getApplication());

    }

}
