package progettomobile.balancemanager.appwidget.configactivity;

public interface AccountSelectedListener {
    void onAccountSelected(long accountId);
}
