package progettomobile.balancemanager.appwidget.configactivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.database.entities.Account;

public class AppWidgetAccountListItemAdapter extends RecyclerView.Adapter<AppWidgetAccountListItemViewHolder> {

    private List<Account> accountList;
    private AccountSelectedListener listener;


    public AppWidgetAccountListItemAdapter(final List<Account> accountList, @NonNull final AccountSelectedListener listener) {
        this.accountList = accountList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public AppWidgetAccountListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.app_widget_account_list_item, parent, false);
        return new AppWidgetAccountListItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AppWidgetAccountListItemViewHolder holder, int position) {
        holder.setAccountName(accountList.get(position).getAccountName());
        holder.setAccountBalance(accountList.get(position).getAccountBalance());
        holder.setTextViewAreaListener(v -> listener.onAccountSelected(accountList.get(position).getIdAccount()));
    }

    @Override
    public int getItemCount() {
        return accountList.size();
    }
}
