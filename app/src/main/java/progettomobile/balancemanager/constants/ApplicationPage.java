package progettomobile.balancemanager.constants;

import progettomobile.balancemanager.R;

public enum ApplicationPage {
    LAST_ACCOUNT(R.string.page_last_account),
    ACCOUNT_LIST(R.string.page_account_list),
    CATEGORY_LIST(R.string.page_category_list),
    SETTINGS(R.string.page_settings),
    TUTORIAL(R.string.page_tutorial),
    CREDITS(R.string.page_credits)
    ;

    public final int stringId;

    ApplicationPage(int stringId) {
        this.stringId = stringId;
    }
}
