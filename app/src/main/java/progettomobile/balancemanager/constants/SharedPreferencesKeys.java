package progettomobile.balancemanager.constants;

import progettomobile.balancemanager.BuildConfig;

public class SharedPreferencesKeys {
    public static final String GENERAL_PREFERENCES = BuildConfig.APPLICATION_ID + "_General";
    public static final String LAST_ACCOUNT = "LAST_ACCOUNT";
    public static final String THEME_PREFERENCE = "THEME_PREFERENCE";
    public static final String CURRENCY_PREFERENCE = "CURRENCY_PREFERENCE";

    public static final String ACCOUNT_PREFIX ="ACCOUNT_";
    public static final String CAN_ACCOUNT_BE_NEGATIVE = "ACCOUNT_NEGATIVE";

    public static final String APP_WIDGET_PREFIX = "APP_WIDGET_";
    public static final String APP_WIDGET_ACCOUNT_ID = "APP_WIDGET_ACCOUNT_ID";

    public static final long NO_CURRENT_ACCOUNT = 0;

    private SharedPreferencesKeys() {}
}
