package progettomobile.balancemanager.constants;

public enum ManagedEntity {
    TRANSACTION,
    TRANSFER,
    ACCOUNT,
    CATEGORY,
    MENU_LIST,
    SETTING_PAGE
}
