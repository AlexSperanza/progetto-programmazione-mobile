package progettomobile.balancemanager.constants;

public class RequestCodes {
    public static final int REQUEST_NEW_ACCOUNT = 1;
    public static final int REQUEST_NEW_CATEGORY = 2;
    public static final int REQUEST_EDIT_ACCOUNT = 3;
    public static final int REQUEST_EDIT_CATEGORY = 4;
    public static final int REQUEST_NEW_TRANSACTION = 5;
    public static final int REQUEST_EDIT_TRANSACTION = 6;

    public static final int REQUEST_SETTINGS = 7;

    private RequestCodes() {}
}
