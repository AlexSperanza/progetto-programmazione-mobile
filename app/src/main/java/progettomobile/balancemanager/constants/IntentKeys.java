package progettomobile.balancemanager.constants;

public class IntentKeys {
    public static final String FIRST_START = "FIRST_START";
    public static final String MANAGED_ENTITY = "MANAGED_ENTITY";
    public static final String PAGE = "PAGE";

    public static final String EDIT_ACCOUNT_ID = "EDIT_ACCOUNT_ID";
    public static final long NO_ACCOUNT_ID = 0L;

    public static final String EDIT_CATEGORY_ID = "EDIT_CATEGORY_ID";
    public static final long NO_CATEGORY_ID = 0L;
    public static final String CALLING_ACTIVITY = "CALLING_ACTIVITY";

    public static final String EDIT_TRANSACTION_ID = "EDIT_TRANSACTION_ID";
    public static final long NO_TRANSACTION_ID = 0L;
    public static final String TRANSACTION_DATE = "NEW_TRANSACTION_DATE";

    public static final String SETTING_ACCOUNT_ID = "SETTING_ACCOUNT_ID";
    public static final long GENERAL_SETTINGS = 0L;

    private IntentKeys() {}
}
