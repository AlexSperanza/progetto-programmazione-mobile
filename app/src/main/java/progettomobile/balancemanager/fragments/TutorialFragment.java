package progettomobile.balancemanager.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import progettomobile.balancemanager.R;

public class TutorialFragment extends Fragment {

    private Activity activity;
    private boolean isFirstStart;

    public TutorialFragment(@NonNull Activity activity, boolean isFirstStart) {
        this.activity = activity;
        this.isFirstStart = isFirstStart;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (container != null) {
            container.removeAllViews();
        }
        View view = inflater.inflate(R.layout.tutorial_fragment, container, false);
        ViewPager2 viewPager = view.findViewById(R.id.tutorial_viewPager);
        TutorialViewPagerAdapter adapter = new TutorialViewPagerAdapter(this);
        Button nextButton = view.findViewById(R.id.tutorial_next_button);
        Button prevButton = view.findViewById(R.id.tutorial_prev_button);

        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                prevButton.setVisibility(position == 0 ? View.INVISIBLE : View.VISIBLE);

                if (position == TutorialPageFragment.getPageCount() - 1 && isFirstStart) {
                    nextButton.setText(R.string.tutorial_close_button_text);
                    nextButton.setOnClickListener(v -> activity.finish());
                } else if (position == TutorialPageFragment.getPageCount() - 1 && !isFirstStart) {
                    nextButton.setVisibility(View.INVISIBLE);
                } else {
                    nextButton.setText(R.string.tutorial_next_button_text);
                    nextButton.setVisibility(View.VISIBLE);
                }
            }
        });

        prevButton.setOnClickListener(v -> viewPager.setCurrentItem(Math.max(viewPager.getCurrentItem() - 1, 0)));
        nextButton.setOnClickListener(v -> viewPager.setCurrentItem(Math.min(viewPager.getCurrentItem() + 1, TutorialPageFragment.getPageCount() - 1)));

        viewPager.setAdapter(adapter);
        return view;
    }

    private class TutorialViewPagerAdapter extends FragmentStateAdapter {

        public TutorialViewPagerAdapter(@NonNull Fragment fragment) {
            super(fragment);
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            return new TutorialPageFragment(position);
        }

        @Override
        public int getItemCount() {
            return TutorialPageFragment.getPageCount();
        }
    }
}
