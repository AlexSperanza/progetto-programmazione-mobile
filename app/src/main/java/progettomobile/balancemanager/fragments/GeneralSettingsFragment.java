package progettomobile.balancemanager.fragments;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.appwidget.providers.SingleAccountAppWidgetProvider;
import progettomobile.balancemanager.constants.SharedPreferencesKeys;

public class GeneralSettingsFragment extends Fragment {

    private static List<String> themeNames = new ArrayList<>();
    private static List<Integer> themeIds = new ArrayList<>();

    private static List<String> currencyValues = new ArrayList<>();

    static {
        themeIds.add(R.style.LightAppTheme);
        themeIds.add(R.style.DarkAppTheme);
    }

    static {
        currencyValues.add("€");
        currencyValues.add("$");
        currencyValues.add("£");
    }

    private Activity activity;

    public GeneralSettingsFragment() {}

    public GeneralSettingsFragment(@NonNull Activity activity) {
        this.activity = activity;
        {
            themeNames.clear();
            themeNames.add(activity.getResources().getString(R.string.light_theme_name));
            themeNames.add(activity.getResources().getString(R.string.dark_theme_name));
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (container != null) {
            container.removeAllViews();
        }
        View view = inflater.inflate(R.layout.general_settings_fragment, container, false);
        initThemeSpinner(view);
        initCurrencySpinner(view);
        return view;
    }

    private AdapterView.OnItemSelectedListener themeListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (themeIds.get(position) != activity.getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE)
                    .getInt(SharedPreferencesKeys.THEME_PREFERENCE, themeIds.get(0))) {
                activity.getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE)
                        .edit()
                        .putInt(SharedPreferencesKeys.THEME_PREFERENCE, themeIds.get(position))
                        .apply();
                activity.setTheme(themeIds.get(position));
                Intent intent = activity.getIntent();
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                activity.finish();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private AdapterView.OnItemSelectedListener currencyListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (!currencyValues.get(position).equals(activity.getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE)
                    .getString(SharedPreferencesKeys.CURRENCY_PREFERENCE, currencyValues.get(0)))) {
                activity.getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE)
                        .edit()
                        .putString(SharedPreferencesKeys.CURRENCY_PREFERENCE, currencyValues.get(position))
                        .apply();
                // Updates widgets on currency symbol change
                Intent intent = new Intent(activity, SingleAccountAppWidgetProvider.class);
                intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                int[] ids = AppWidgetManager.getInstance(activity).getAppWidgetIds(new ComponentName(activity, SingleAccountAppWidgetProvider.class));
                intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
                activity.sendBroadcast(intent);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private void initThemeSpinner (@NonNull View view) {
        Spinner spinner = view.findViewById(R.id.theme_spinner);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAdapter.addAll(themeNames);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(themeListener);
        int currentThemeId = activity.getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE)
                .getInt(SharedPreferencesKeys.THEME_PREFERENCE, themeIds.get(0));

        for (int i = 0; i < themeIds.size(); i++) {
            if (currentThemeId == themeIds.get(i)) {
                spinner.setSelection(i);
                break;
            }
        }
    }

    private void initCurrencySpinner (@NonNull View view) {
        Spinner spinner = view.findViewById(R.id.currency_spinner);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAdapter.addAll(currencyValues);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(currencyListener);
        String currentCurrency = activity.getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE)
                .getString(SharedPreferencesKeys.CURRENCY_PREFERENCE, currencyValues.get(0));

        for (int i = 0; i < currencyValues.size(); i++) {
            if (currentCurrency.equals(currencyValues.get(i))) {
                spinner.setSelection(i);
                break;
            }
        }
    }
}
