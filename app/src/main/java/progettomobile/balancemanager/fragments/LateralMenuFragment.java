package progettomobile.balancemanager.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.activities.HomeActivity;
import progettomobile.balancemanager.activities.SimpleActivity;
import progettomobile.balancemanager.constants.ApplicationPage;
import progettomobile.balancemanager.constants.IntentKeys;
import progettomobile.balancemanager.recyclerviews.OnItemClickListener;
import progettomobile.balancemanager.recyclerviews.adapters.LateralMenuItemAdapter;

public class LateralMenuFragment extends Fragment implements OnItemClickListener {

    private LateralMenuItemAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (container != null) {
            container.removeAllViews();
        }
        View view = inflater.inflate(R.layout.recycler_view_fragment, container, false);
        setupRecyclerView(view);
        return view;
    }

    @Override
    public void onItemClick(int position) {
        sendIntentFor(adapter.getPageList().get(position));
    }

    private void setupRecyclerView(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new LateralMenuItemAdapter(this);
        recyclerView.setAdapter(adapter);
    }

    private void sendIntentFor(@NonNull ApplicationPage page) {
        Activity activity = getActivity();
        Class<? extends AppCompatActivity> activityClass;
        if (activity != null) {
            if (page == ApplicationPage.SETTINGS
                    || page == ApplicationPage.CREDITS
                    || page == ApplicationPage.TUTORIAL) { // activity con solo pulsante Menù
                activityClass = SimpleActivity.class;
            } else { // Activity con pulsante menù e pulsante add
                activityClass = HomeActivity.class;
            }
            Intent intent = new Intent(activity, activityClass);
            intent.putExtra(IntentKeys.PAGE, page);
            intent.setFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            activity.finish();
        }
    }
}
