package progettomobile.balancemanager.fragments;

import android.view.View;

public interface AddButtonListener {
    /**
     * Return the callback function to use when the add button is clicked.
     * @return the callback function.
     */
    View.OnClickListener getAddButtonListener();
}
