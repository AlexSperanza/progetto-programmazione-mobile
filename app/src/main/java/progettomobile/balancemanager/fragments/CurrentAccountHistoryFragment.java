package progettomobile.balancemanager.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.calendarview.basics.Date;
import progettomobile.balancemanager.calendarview.datepicker.DatePickerFragment;
import progettomobile.balancemanager.calendarview.listeners.OnDateSelectedListener;
import progettomobile.balancemanager.calendarview.views.CalendarView;
import progettomobile.balancemanager.database.Database;
import progettomobile.balancemanager.recyclerviews.adapters.TransactionListItemAdapter;

import org.joda.time.LocalDate;

import java.util.ArrayList;

public class CurrentAccountHistoryFragment extends Fragment {

    private Activity activity;

    public CurrentAccountHistoryFragment(@NonNull Activity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.current_account_history_tab_fragment, container, false);
        if (activity == null && getActivity() != null) {
            activity = getActivity();
        }
        if (activity != null) {
            initRecyclerView(view);
            CalendarView calendarView = view.findViewById(R.id.history_calendarview);
            calendarView.setOnDateSelectedListener(new OnDateSelectedListener() {
                @Override
                public void onDateSelected(Date date) {
                    changeRecyclerAdapter(view, date.toJodaDate());
                }
            });
            calendarView.setCurrentMonthOnClickListeners(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatePickerFragment datePicker = new DatePickerFragment();
                    datePicker.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            calendarView.scrollToDate(year, month + 1, dayOfMonth, false);
                        }
                    });
                    datePicker.show(getChildFragmentManager(), "DatePicker");
                }
            });
        }
        return view;
    }

    private void initRecyclerView(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.day_transaction_recyclerView);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);
        changeRecyclerAdapter(view, LocalDate.now());
    }

    private void changeRecyclerAdapter(View view, LocalDate date) {
        TransactionListItemAdapter transactionListItemAdapter = new TransactionListItemAdapter(Database.getCurrentAccountTransactions().containsKey(date) ? Database.getCurrentAccountTransactions().get(date) : new ArrayList<>(), getActivity());
        ((RecyclerView) view.findViewById(R.id.day_transaction_recyclerView)).setAdapter(transactionListItemAdapter);
    }

}
