package progettomobile.balancemanager.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.database.Database;
import progettomobile.balancemanager.database.entities.Account;
import progettomobile.balancemanager.recyclerviews.adapters.SettingSectionItemAdapter;

import java.util.ArrayList;
import java.util.List;

public class SettingSectionListFragment extends Fragment {

    private @NonNull Activity activity;

    public SettingSectionListFragment(@NonNull Activity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (container != null) {
            container.removeAllViews();
        }
        View view = inflater.inflate(R.layout.recycler_view_fragment, container, false);
        initRecyclerView(view);
        return view;
    }

    private void initRecyclerView(View view) {
        List<Account> accountList = new ArrayList<>(Database.getAccountList());
        Account mockAccount = new Account();
        mockAccount.setIdAccount(0);
        accountList.add(0, mockAccount);
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);
        SettingSectionItemAdapter adapter = new SettingSectionItemAdapter(accountList, activity);
        recyclerView.setAdapter(adapter);
    }
}
