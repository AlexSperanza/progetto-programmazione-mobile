package progettomobile.balancemanager.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import progettomobile.balancemanager.R;

import java.util.ArrayList;
import java.util.List;

public class TutorialPageFragment extends Fragment {

    private int pageNumber;

    public TutorialPageFragment(int position) {
        this.pageNumber = position;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(pageIds.get(pageNumber), container, false);
    }

    private final static List<Integer> pageIds = new ArrayList<>();

    static {
        pageIds.add(R.layout.tutorial_page1_fragment);
        pageIds.add(R.layout.tutorial_page2_fragment);
        pageIds.add(R.layout.tutorial_page3_fragment);
        pageIds.add(R.layout.tutorial_page4_fragment);
        pageIds.add(R.layout.tutorial_page5_fragment);
        pageIds.add(R.layout.tutorial_page6_fragment);
        pageIds.add(R.layout.tutorial_page7_fragment);
        pageIds.add(R.layout.tutorial_page8_fragment);
        pageIds.add(R.layout.tutorial_page9_fragment);
    }

    static public int getPageCount() {
        return pageIds.size();
    }

}
