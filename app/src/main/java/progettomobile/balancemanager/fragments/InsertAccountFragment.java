package progettomobile.balancemanager.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.activities.HomeActivity;
import progettomobile.balancemanager.constants.ApplicationPage;
import progettomobile.balancemanager.constants.IntentKeys;
import progettomobile.balancemanager.constants.SharedPreferencesKeys;
import progettomobile.balancemanager.database.Database;
import progettomobile.balancemanager.database.entities.Account;
import progettomobile.balancemanager.database.entities.Transaction;
import progettomobile.balancemanager.database.entities.TransactionBuilder;
import progettomobile.balancemanager.utils.BalanceUtils;

import org.joda.time.LocalDate;

public class InsertAccountFragment extends Fragment implements DeleteButtonListener {

    private Activity activity;

    private Account newAccount;
    private boolean isModify;

    public InsertAccountFragment() {
    }

    private InsertAccountFragment(@NonNull Activity activity) {
        this.activity = activity;
    }

    public InsertAccountFragment(@NonNull Activity activity, Account account) {
        this(activity);
        if (account != null) {
            this.newAccount = account;
            isModify = true;
        } else {
            this.newAccount = new Account();
            isModify = false;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if(container != null) {
            container.removeAllViews();
        }
        View view = inflater.inflate(R.layout.insert_account_fragment, container, false);
        if (activity == null && getActivity() != null) {
            activity = getActivity();
        }
        if (isModify) {
            ((EditText)view.findViewById(R.id.account_name_editText)).setText(newAccount.getAccountName());
            ((EditText)view.findViewById(R.id.account_balance_editText)).setText(BalanceUtils.balanceStringFromInt(newAccount.getAccountBalance()));
        }

        view.findViewById(R.id.save_account_info_button).setOnClickListener(v -> {
            if (isModify) {
                onModifyAccount(view);
            } else {
                onInsertAccount(view);
            }
        });

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                view.findViewById(R.id.save_account_info_button).setEnabled(isFormCompiled(view));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        ((EditText) view.findViewById(R.id.account_name_editText)).addTextChangedListener(textWatcher);
        ((EditText) view.findViewById(R.id.account_balance_editText)).addTextChangedListener(textWatcher);

        view.findViewById(R.id.save_account_info_button).setEnabled(isFormCompiled(view));

        return view;
    }

    @Override
    public View.OnClickListener getDeleteButtonListener() {
        return v -> {
            if (activity != null) {
                AlertDialog dialog = new AlertDialog.Builder(activity)
                        .setMessage(R.string.confirmation_dialog_account_message)
                        .setTitle(R.string.confirmation_dialog_title)
                        .setNegativeButton(R.string.confirmation_dialog_negative_button, (dialog1, which) -> dialog1.dismiss())
                        .setPositiveButton(R.string.confirmation_dialog_positive_button, (dialog1, which) -> {
                            if (activity != null) {
                                Intent returnIntent = new Intent(activity, HomeActivity.class);
                                returnIntent.putExtra(IntentKeys.PAGE, ApplicationPage.ACCOUNT_LIST);
                                activity.setResult(Activity.RESULT_OK, returnIntent);
                                activity.finish();
                            }
                            Database.deleteAccount(newAccount);
                        })
                        .create();
                dialog.show();
            }
        };
    }

    private boolean isFormCompiled(View view) {
        return ((EditText) view.findViewById(R.id.account_name_editText)).getText().length() > 0
                && ((EditText) view.findViewById(R.id.account_balance_editText)).getText().length() > 0;
    }

    private void onInsertAccount(View view) {
        long accountId = Database.getLastAccountId() + 1;
        newAccount.setIdAccount(accountId);
        String newName = ((EditText) view.findViewById(R.id.account_name_editText)).getText().toString();
        newAccount.setAccountName(newName);
        int newBalance = BalanceUtils.parseBalanceValue(((EditText) view.findViewById(R.id.account_balance_editText)).getText().toString());
        newAccount.setAccountBalance(newBalance);
        Transaction transaction = new TransactionBuilder()
                                        .setTransactionId(Database.getLastTransactionId() + 1)
                                        .setAccountId(newAccount.getIdAccount())
                                        .setCategoryId(1)
                                        .setDescription(getString(R.string.initial_balance_transaction_text))
                                        .setDate(LocalDate.now())
                                        .setValue(newBalance)
                                        .setTargetAccountId(0)
                                        .build();
        Database.insertAccount(newAccount);
        Database.insertTransaction(transaction);
        if (activity != null) {
            activity.getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE)
                    .edit()
                    .putLong(SharedPreferencesKeys.LAST_ACCOUNT, newAccount.getIdAccount())
                    .apply();
            Intent returnIntent = new Intent(activity, HomeActivity.class);
            returnIntent.putExtra(IntentKeys.PAGE, ApplicationPage.LAST_ACCOUNT);
            activity.setResult(Activity.RESULT_OK, returnIntent);
            activity.finish();
        }
    }

    private void onModifyAccount(View view) {
        String newName = ((EditText) view.findViewById(R.id.account_name_editText)).getText().toString();
        if (!newAccount.getAccountName().equals(newName)) {
            newAccount.setAccountName(newName);
        }
        int newBalance = BalanceUtils.parseBalanceValue(((EditText) view.findViewById(R.id.account_balance_editText)).getText().toString());
        if (newAccount.getAccountBalance() != newBalance) {
            int value = newBalance - newAccount.getAccountBalance();
            Transaction transaction = new TransactionBuilder()
                                            .setAccountId(newAccount.getIdAccount())
                                            .setTransactionId(Database.getLastTransactionId() + 1)
                                            .setCategoryId(2)
                                            .setDescription(getString(R.string.balance_correction_transaction_text))
                                            .setDate(LocalDate.now())
                                            .setValue(value)
                                            .setTargetAccountId(0)
                                            .setTargetTransactionId(0)
                                            .build();
            newAccount.setAccountBalance(newBalance);
            Database.insertTransaction(transaction);
        }
        Database.updateAccount(newAccount);
        if (activity != null) {
            Intent returnIntent = new Intent(activity, HomeActivity.class);
            returnIntent.putExtra(IntentKeys.PAGE, ApplicationPage.ACCOUNT_LIST);
            activity.setResult(Activity.RESULT_OK, returnIntent);
            activity.finish();
        }
    }
}
