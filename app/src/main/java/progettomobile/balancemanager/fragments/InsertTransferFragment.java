package progettomobile.balancemanager.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.activities.HomeActivity;
import progettomobile.balancemanager.constants.ApplicationPage;
import progettomobile.balancemanager.constants.IntentKeys;
import progettomobile.balancemanager.constants.SharedPreferencesKeys;
import progettomobile.balancemanager.database.Database;
import progettomobile.balancemanager.database.entities.Account;
import progettomobile.balancemanager.database.entities.Transaction;
import progettomobile.balancemanager.utils.BalanceUtils;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;

public class InsertTransferFragment extends Fragment implements DeleteButtonListener, AdapterView.OnItemSelectedListener {
    private Account currentAccount;
    private Transaction transfer;
    private boolean isModify;
    private List<Account> accountList;
    private int selectedAccountPosition = 0;

    private Activity activity;

    public InsertTransferFragment() {

    }

    private InsertTransferFragment(@NonNull Activity activity) {
        this.activity = activity;
    }

    public InsertTransferFragment(@NonNull Activity activity, @NonNull LocalDate date) {
        this(activity);
        transfer = new Transaction();
        transfer.setDate(date);
        isModify = false;
    }

    public InsertTransferFragment(@NonNull Activity activity, Transaction transfer) {
        this(activity);
        this.transfer = transfer;
        isModify = true;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (container != null) {
            container.removeAllViews();
        }
        View view = inflater.inflate(R.layout.insert_transfer_fragment, container, false);
        if (activity == null && getActivity() != null) {
            activity = getActivity();
        }
        if (activity != null) {
            currentAccount = Database.findAccount(activity.getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE).getLong(SharedPreferencesKeys.LAST_ACCOUNT, SharedPreferencesKeys.NO_CURRENT_ACCOUNT));
            if (currentAccount != null) {
                String accountName = currentAccount.getAccountName();
                ((TextView) view.findViewById(R.id.transfer_fromAccount_textView)).setText(getString(R.string.transfer_from_account_text, accountName));
            }
            setupTargetAccountSpinner(view, activity);
            ((Spinner) view.findViewById(R.id.transfer_toAccount_spinner)).setOnItemSelectedListener(this);
            ((TextView) view.findViewById(R.id.transfer_date_textView)).setText(transfer.getDate().toString());
            view.findViewById(R.id.save_button).setOnClickListener(v -> {
                if (isModify) {
                    onModify(view);
                } else {
                    onInsert(view);
                }
            });

            TextWatcher textWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    view.findViewById(R.id.save_button).setEnabled(isFormCompiled(view));
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            };

            ((EditText) view.findViewById(R.id.transfer_description_editText)).addTextChangedListener(textWatcher);
            ((EditText) view.findViewById(R.id.transfer_value_editText)).addTextChangedListener(textWatcher);
            view.findViewById(R.id.save_button).setEnabled(isModify);
            if (isModify) {
                ((EditText) view.findViewById(R.id.transfer_description_editText)).setText(transfer.getDescription());
                ((EditText) view.findViewById(R.id.transfer_value_editText)).setText(BalanceUtils.balanceStringFromInt(-transfer.getCentValue()));
                int i = 0;
                boolean found = false;
                for (; i < accountList.size() && !found; i++) {
                    if (accountList.get(i).getIdAccount() == transfer.getTargetAccountId()) {
                        found = true;
                        ((Spinner) view.findViewById(R.id.transfer_toAccount_spinner)).setSelection(i);
                    }
                }
            }
        }
        return view;
    }

    @Override
    public View.OnClickListener getDeleteButtonListener() {
        return v -> {
            Context context = getContext();
            if (context != null) {
                AlertDialog dialog = new AlertDialog.Builder(context)
                        .setMessage(R.string.confirmation_dialog_transaction_message)
                        .setTitle(R.string.confirmation_dialog_title)
                        .setNegativeButton(R.string.confirmation_dialog_negative_button, (dialog1, which) -> dialog1.dismiss())
                        .setPositiveButton(R.string.confirmation_dialog_positive_button, (dialog1, which) -> {
                            Account account = Database.findAccount(transfer.getAccountId());
                            if (account != null) {
                                account.setAccountBalance(account.getAccountBalance() - transfer.getCentValue());
                            }
                            Account targetAccount = Database.findAccount(transfer.getTargetAccountId());
                            if (targetAccount != null) {
                                targetAccount.setAccountBalance(targetAccount.getAccountBalance() + transfer.getCentValue());
                            }
                            Database.deleteTransaction(transfer);
                            Database.deleteCounterTransfer(transfer);
                            if (activity != null) {
                                Intent returnIntent = new Intent(activity, HomeActivity.class);
                                returnIntent.putExtra(IntentKeys.PAGE, ApplicationPage.LAST_ACCOUNT);
                                activity.setResult(Activity.RESULT_OK, returnIntent);
                                activity.finish();
                            }
                        })
                        .create();
                dialog.show();
            }
        };
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedAccountPosition = position;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void onInsert(View view) {
        if (activity != null) {
            int value = -BalanceUtils.parseBalanceValue(((EditText)view.findViewById(R.id.transfer_value_editText)).getText().toString());
            String desc = ((EditText) view.findViewById(R.id.transfer_description_editText)).getText().toString();
            if (currentAccount.getAccountBalance() + value < 0 && !canAccountBeNegative(currentAccount.getIdAccount(), activity)) {
                showNegativityDialog(activity, BalanceUtils.balanceStringFromInt(currentAccount.getAccountBalance()), currentAccount.getAccountName());
            } else {
                Account targetAccount = accountList.get(selectedAccountPosition);

                transfer.setAccountId(currentAccount.getIdAccount());
                transfer.setIdTransaction(Database.getLastTransactionId() + 1);
                transfer.setTargetAccountId(targetAccount.getIdAccount());
                currentAccount.setAccountBalance(currentAccount.getAccountBalance() + value);
                transfer.setCentValue(value);
                transfer.setDescription(desc);
                transfer.setCategoryId(3); // 3 = TRANSFER

                Transaction counterTransfer = new Transaction();
                counterTransfer.setAccountId(targetAccount.getIdAccount());
                counterTransfer.setIdTransaction(Database.getLastTransactionId() + 2);
                transfer.setTargetTransactionId(counterTransfer.getIdTransaction());
                counterTransfer.setCategoryId(3);
                counterTransfer.setCentValue(-value);
                counterTransfer.setDate(transfer.getDate());
                targetAccount.setAccountBalance(targetAccount.getAccountBalance() - value);
                counterTransfer.setDescription(desc);
                counterTransfer.setTargetAccountId(currentAccount.getIdAccount());
                counterTransfer.setTargetTransactionId(transfer.getIdTransaction());

                Database.insertTransaction(transfer);
                Database.insertTransaction(counterTransfer);

                Intent returnIntent = new Intent(activity, HomeActivity.class);
                returnIntent.putExtra(IntentKeys.PAGE, ApplicationPage.LAST_ACCOUNT);
                activity.setResult(Activity.RESULT_OK, returnIntent);
                activity.finish();
            }
        }
    }

    private void onModify(View view) {
        if (activity != null) {
            String newDesc = ((EditText) view.findViewById(R.id.transfer_description_editText)).getText().toString();
            int newValue = -BalanceUtils.parseBalanceValue(((EditText) view.findViewById(R.id.transfer_value_editText)).getText().toString());
            Account newTargetAccount = accountList.get(selectedAccountPosition);
            if (currentAccount.getAccountBalance() - newValue < 0 && !canAccountBeNegative(currentAccount.getIdAccount(), activity)) {
                showNegativityDialog(activity, BalanceUtils.balanceStringFromInt((currentAccount.getAccountBalance() - transfer.getCentValue())), currentAccount.getAccountName());
            } else {
                if (!transfer.getDescription().equals(newDesc) || transfer.getCentValue() != newValue || transfer.getTargetAccountId() != newTargetAccount.getIdAccount()) {
                    if (!transfer.getDescription().equals(newDesc)) {
                        transfer.setDescription(newDesc);
                    }
                    if (transfer.getTargetAccountId() != newTargetAccount.getIdAccount() || transfer.getCentValue() != newValue) {
                        Account oldAccount = Database.findAccount(transfer.getTargetAccountId());
                        int oldValue = transfer.getCentValue();
                        transfer.setCentValue(newValue);
                        currentAccount.setAccountBalance(currentAccount.getAccountBalance() - oldValue + newValue);
                        newTargetAccount.setAccountBalance(newTargetAccount.getAccountBalance() - newValue);
                        if (oldAccount != null && oldAccount.getIdAccount() != newTargetAccount.getIdAccount()) {
                            transfer.setTargetAccountId(newTargetAccount.getIdAccount());
                        }
                        if (oldAccount != null) {
                            oldAccount.setAccountBalance(oldAccount.getAccountBalance() + oldValue);
                            Database.updateAccount(oldAccount);
                        }
                    }

                    Transaction counterTransfer = new Transaction();
                    counterTransfer.setAccountId(transfer.getTargetAccountId());
                    counterTransfer.setIdTransaction(transfer.getTargetTransactionId());
                    counterTransfer.setCategoryId(3);
                    counterTransfer.setCentValue(-newValue);
                    counterTransfer.setDate(transfer.getDate());
                    counterTransfer.setDescription(newDesc);
                    counterTransfer.setTargetAccountId(transfer.getAccountId());
                    counterTransfer.setTargetTransactionId(transfer.getIdTransaction());

                    Database.updateTransaction(transfer);
                    Database.updateTransaction(counterTransfer);
                    Database.updateAccount(currentAccount);
                    Database.updateAccount(newTargetAccount);
                }
                Intent returnIntent = new Intent(activity, HomeActivity.class);
                returnIntent.putExtra(IntentKeys.PAGE, ApplicationPage.LAST_ACCOUNT);
                activity.setResult(Activity.RESULT_OK, returnIntent);
                activity.finish();
            }
        }
    }

    private void setupTargetAccountSpinner(View view, Activity activity) {
        accountList = new ArrayList<>(Database.getAccountList());
        accountList.remove(currentAccount);
        final List<String> spinnerValues = new ArrayList<>();
        for (Account account :
                accountList) {
            spinnerValues.add(account.getAccountName());
        }
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAdapter.addAll(spinnerValues);
        ((Spinner)view.findViewById(R.id.transfer_toAccount_spinner)).setAdapter(spinnerAdapter);
    }

    private boolean canAccountBeNegative(long accountId, Context context) {
        return context.getSharedPreferences(SharedPreferencesKeys.ACCOUNT_PREFIX + accountId, Context.MODE_PRIVATE)
                .getBoolean(SharedPreferencesKeys.CAN_ACCOUNT_BE_NEGATIVE, true);
    }

    private void showNegativityDialog(Context context, String maxPossibleValue, String accountName) {
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setMessage(getString(R.string.alert_dialog_account_negative_message, maxPossibleValue, accountName))
                .setTitle(R.string.alert_dialog_title)
                .setNegativeButton(R.string.alert_dialog_dismiss_text, (dialog1, which) -> dialog1.dismiss())
                .create();
        dialog.show();
    }

    private boolean isFormCompiled(View view) {
        return ((EditText) view.findViewById(R.id.transfer_description_editText)).getText().length() > 0
                && ((EditText) view.findViewById(R.id.transfer_value_editText)).getText().length() > 0
                && BalanceUtils.parseBalanceValue(((EditText) view.findViewById(R.id.transfer_value_editText)).getText().toString()) != 0;
    }
}
