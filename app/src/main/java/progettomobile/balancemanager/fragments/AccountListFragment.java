package progettomobile.balancemanager.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.activities.BackableActivity;
import progettomobile.balancemanager.constants.IntentKeys;
import progettomobile.balancemanager.constants.ManagedEntity;
import progettomobile.balancemanager.constants.RequestCodes;
import progettomobile.balancemanager.database.Database;
import progettomobile.balancemanager.recyclerviews.adapters.AccountListItemAdapter;

public class AccountListFragment extends Fragment implements AddButtonListener {

    private Activity activity;

    public AccountListFragment() {}

    public AccountListFragment(@NonNull Activity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (container != null) {
            container.removeAllViews();
        }
        View view = inflater.inflate(R.layout.recycler_view_fragment, container, false);
        if (activity == null && getActivity() != null) {
            activity = getActivity();
        }
        setupRecyclerView(view);
        return view;
    }

    @Override
    public View.OnClickListener getAddButtonListener() {
        return v -> {
            if (activity != null) {
                Intent intent = new Intent(activity, BackableActivity.class);
                intent.putExtra(IntentKeys.MANAGED_ENTITY, ManagedEntity.ACCOUNT);
                startActivityForResult(intent, RequestCodes.REQUEST_NEW_ACCOUNT);
            }
        };
    }

    private void setupRecyclerView(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);
        AccountListItemAdapter adapter = new AccountListItemAdapter(Database.getAccountList(), getActivity());
        recyclerView.setAdapter(adapter);
    }
}
