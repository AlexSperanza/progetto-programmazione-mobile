package progettomobile.balancemanager.fragments;

import android.view.View;

public interface DeleteButtonListener {
    /**
     * Returns the callback function to use when the delete button is clicked.
     * @return the callback function.
     */
    View.OnClickListener getDeleteButtonListener();
}
