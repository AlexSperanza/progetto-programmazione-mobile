package progettomobile.balancemanager.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.activities.HomeActivity;
import progettomobile.balancemanager.constants.ApplicationPage;
import progettomobile.balancemanager.constants.IntentKeys;
import progettomobile.balancemanager.database.Database;
import progettomobile.balancemanager.database.entities.Category;
import progettomobile.balancemanager.database.entities.CategoryBuilder;

import java.util.Objects;

public class InsertCategoryFragment extends Fragment implements DeleteButtonListener {

    private Category category;
    private boolean isModify;

    public InsertCategoryFragment() {

    }

    public InsertCategoryFragment(Category category) {
        if (category != null) {
            this.category = category;
            isModify = true;
        } else {
            this.category = new Category();
            isModify = false;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (container != null) {
            container.removeAllViews();
        }

        View view = inflater.inflate(R.layout.insert_category_fragment, container, false);
        view.findViewById(R.id.insert_category_button).setOnClickListener(v -> {
            if (isModify) {
                onModifyCategory(view);
            } else {
                onInsertCategory(view);
            }
        });

        view.findViewById(R.id.insert_category_button).setEnabled(false);

        ((EditText) view.findViewById(R.id.category_name_editText)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                view.findViewById(R.id.insert_category_button).setEnabled(s.length() > 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if (isModify) {
            ((EditText) view.findViewById(R.id.category_name_editText)).setText(category.getCategoryName());
        }

        return view;
    }

    @Override
    public View.OnClickListener getDeleteButtonListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = getContext();
                if (context != null) {
                    AlertDialog dialog = new AlertDialog.Builder(context)
                            .setMessage(R.string.confirmation_dialog_category_message)
                            .setTitle(R.string.confirmation_dialog_title)
                            .setNegativeButton(R.string.confirmation_dialog_negative_button, (dialog1, which) -> dialog1.dismiss())
                            .setPositiveButton(R.string.confirmation_dialog_positive_button, (dialog1, which) -> {
                                Activity activity = getActivity();
                                if (activity != null) {
                                    Intent returnIntent = new Intent(activity, HomeActivity.class);
                                    returnIntent.putExtra(IntentKeys.PAGE, ApplicationPage.CATEGORY_LIST);
                                    activity.setResult(Activity.RESULT_OK, returnIntent);
                                    activity.finish();
                                }
                                Database.deleteCategory(category);
                            })
                            .create();
                    dialog.show();
                }
            }
        };
    }

    private void onInsertCategory(View view) {
        Activity activity = getActivity();
        if (activity != null) {
            Category category = new CategoryBuilder()
                    .useFirstFreeId()
                    .setCategoryName(((EditText)view.findViewById(R.id.category_name_editText)).getText().toString())
                    .setUserCategory().build();
            Database.insertCategory(category);
            try {
                Intent returnIntent = new Intent(activity, Class.forName(activity.getIntent().getStringExtra(IntentKeys.CALLING_ACTIVITY)));
                if (Objects.equals(activity.getIntent().getStringExtra(IntentKeys.CALLING_ACTIVITY), HomeActivity.class.getName())) {
                    returnIntent.putExtra(IntentKeys.PAGE, ApplicationPage.CATEGORY_LIST);
                }
                activity.setResult(Activity.RESULT_OK, returnIntent);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                activity.setResult(Activity.RESULT_CANCELED, null);
            } finally {
                activity.finish();
            }
        }
    }

    private void onModifyCategory(View view) {
        Activity activity = getActivity();
        String newName = ((EditText) view.findViewById(R.id.category_name_editText)).getText().toString();
        if (!category.getCategoryName().equals(newName)) {
            category.setCategoryName(newName);
            Database.updateCategory(category);
        }
        if (activity != null) {
            Intent returnIntent = new Intent(activity, HomeActivity.class);
            returnIntent.putExtra(IntentKeys.PAGE, ApplicationPage.CATEGORY_LIST);
            activity.setResult(Activity.RESULT_OK, returnIntent);
            activity.finish();
        }
    }

}
