package progettomobile.balancemanager.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.activities.BackableActivity;
import progettomobile.balancemanager.activities.HomeActivity;
import progettomobile.balancemanager.constants.ApplicationPage;
import progettomobile.balancemanager.constants.IntentKeys;
import progettomobile.balancemanager.constants.ManagedEntity;
import progettomobile.balancemanager.constants.RequestCodes;
import progettomobile.balancemanager.constants.SharedPreferencesKeys;
import progettomobile.balancemanager.database.Database;
import progettomobile.balancemanager.database.entities.Account;
import progettomobile.balancemanager.database.entities.Category;
import progettomobile.balancemanager.database.entities.Transaction;
import progettomobile.balancemanager.utils.BalanceUtils;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;

public class InsertTransactionFragment extends Fragment implements DeleteButtonListener, AdapterView.OnItemSelectedListener {

    private static final int UNCHOOSABLE_CATEGORY_OFFSET = 3;

    private boolean isModify;
    private Transaction transaction;
    private List<Category> categoryList;
    private long selectedCategoryId;

    private Activity activity;

    public InsertTransactionFragment() {

    }

    private InsertTransactionFragment(@NonNull Activity activity) {
        this.activity = activity;
    }

    /**
     *
     * @param date the date on which the transaction is being inserted
     */
    public InsertTransactionFragment(@NonNull Activity activity, LocalDate date) {
        this(activity);
        transaction = new Transaction();
        transaction.setDate(date);
        isModify = false;
    }

    public InsertTransactionFragment(@NonNull Activity activity, Transaction transaction) {
        this(activity);
        this.transaction = transaction;
        isModify = true;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (container != null) {
            container.removeAllViews();
        }
        this.categoryList = Database.getCategoryList();
        View view = inflater.inflate(R.layout.insert_transaction_fragment, container, false);
        if (activity == null && getActivity() != null) {
            activity = getActivity();
        }
        if (activity != null) {
            initRecyclerView(view);
            ((Spinner)view.findViewById(R.id.transaction_category_spinner)).setOnItemSelectedListener(this);
            view.findViewById(R.id.transaction_category_add_icon).setOnClickListener(v -> {
                Intent intent = new Intent(activity, BackableActivity.class);
                intent.putExtra(IntentKeys.MANAGED_ENTITY, ManagedEntity.CATEGORY);
                intent.putExtra(IntentKeys.CALLING_ACTIVITY, BackableActivity.class.getName());
                startActivityForResult(intent, RequestCodes.REQUEST_NEW_CATEGORY);
            });
            ((TextView) view.findViewById(R.id.transaction_date_textView)).setText(transaction.getDate().toString());
            view.findViewById(R.id.transaction_save_button).setOnClickListener(v -> {
                if (isModify) {
                    onModify(view);
                } else {
                    onInsert(view);
                }
            });
        }

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                view.findViewById(R.id.transaction_save_button).setEnabled(isFormCompiled(view));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        ((EditText) view.findViewById(R.id.transaction_desc_editText)).addTextChangedListener(textWatcher);
        ((EditText) view.findViewById(R.id.transaction_value_editText)).addTextChangedListener(textWatcher);

        if (!isModify) {
            view.findViewById(R.id.transaction_save_button).setEnabled(false);
        }

        if (isModify) {
            if (transaction.getCategoryId() == 2) { //Transaction is a balance correction
                view.findViewById(R.id.transaction_desc_editText).setEnabled(false);
                view.findViewById(R.id.transaction_category_spinner).setEnabled(false);
                ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
                spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerAdapter.add(getString(R.string.transfer_category_title));
                ((Spinner)view.findViewById(R.id.transaction_category_spinner)).setAdapter(spinnerAdapter);
            } else {
                ((Spinner) view.findViewById(R.id.transaction_category_spinner)).setSelection((int) transaction.getCategoryId() - UNCHOOSABLE_CATEGORY_OFFSET - 1);
            }
            ((EditText) view.findViewById(R.id.transaction_desc_editText)).setText(transaction.getDescription());
            ((EditText) view.findViewById(R.id.transaction_value_editText)).setText(BalanceUtils.balanceStringFromInt(transaction.getCentValue()));
        }

        return view;
    }

    @Override
    public View.OnClickListener getDeleteButtonListener() {
        return v -> {
            Context context = getContext();
            if (context != null) {
                AlertDialog dialog = new AlertDialog.Builder(context)
                        .setMessage(R.string.confirmation_dialog_transaction_message)
                        .setTitle(R.string.confirmation_dialog_title)
                        .setNegativeButton(R.string.confirmation_dialog_negative_button, (dialog1, which) -> dialog1.dismiss())
                        .setPositiveButton(R.string.confirmation_dialog_positive_button, (dialog1, which) -> {
                            if (activity != null) {
                                Intent returnIntent = new Intent(activity, HomeActivity.class);
                                returnIntent.putExtra(IntentKeys.PAGE, ApplicationPage.LAST_ACCOUNT);
                                activity.setResult(Activity.RESULT_OK, returnIntent);
                                activity.finish();
                            }
                            Account account = Database.findAccount(transaction.getAccountId());
                            if (account != null) {
                                account.setAccountBalance(account.getAccountBalance() - transaction.getCentValue());
                            }
                            Database.deleteTransaction(transaction);
                        })
                        .create();
                dialog.show();
            }
        };
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedCategoryId = categoryList.get(position + UNCHOOSABLE_CATEGORY_OFFSET).getIdCategory();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        View view = getView();
        if (activity != null && view != null) {
            initRecyclerView(view);
            ((Spinner) view.findViewById(R.id.transaction_category_spinner)).setSelection((int) selectedCategoryId - UNCHOOSABLE_CATEGORY_OFFSET - 1);
        }
    }

    private boolean isFormCompiled(View view) {
        return ((EditText) view.findViewById(R.id.transaction_desc_editText)).getText().length() > 0
                && ((EditText) view.findViewById(R.id.transaction_value_editText)).getText().length() > 0
                && BalanceUtils.parseBalanceValue(((EditText) view.findViewById(R.id.transaction_value_editText)).getText().toString()) != 0;
    }

    private void initRecyclerView (View view) {
        List<String> stringList = new ArrayList<>();
        for (Category c : categoryList) {
            if (!c.isUnchoosable()) {
                if (Category.DEFAULT_STRING_MAP.containsKey(c.getCategoryName())) {
                    stringList.add(getResources().getString(Category.DEFAULT_STRING_MAP.get(c.getCategoryName())));
                } else {
                    stringList.add(c.getCategoryName());
                }
            }
        }
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAdapter.addAll(stringList);
        ((Spinner)view.findViewById(R.id.transaction_category_spinner)).setAdapter(spinnerAdapter);
    }

    private void onInsert(View view) {
        if (activity != null) {
            long accountId = activity.getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE)
                    .getLong(SharedPreferencesKeys.LAST_ACCOUNT, SharedPreferencesKeys.NO_CURRENT_ACCOUNT);
            int value = BalanceUtils.parseBalanceValue(((EditText)view.findViewById(R.id.transaction_value_editText)).getText().toString());
            Account account = Database.findAccount(accountId);
            if (account != null && account.getAccountBalance() + value < 0 && !canAccountBeNegative(accountId, activity)) {
                showNegativityDialog(activity, BalanceUtils.balanceStringFromInt(-account.getAccountBalance()), account.getAccountName());
            } else {
                transaction.setAccountId(accountId);
                transaction.setIdTransaction(Database.getLastTransactionId() + 1);

                // This is not a transfer, so the target account is always NULL, as the target transaction
                transaction.setTargetAccountId(0);
                transaction.setTargetTransactionId(0);

                if (account != null) {
                    account.setAccountBalance(account.getAccountBalance() + value);
                }
                transaction.setCentValue(value);
                String desc = ((EditText) view.findViewById(R.id.transaction_desc_editText)).getText().toString();
                transaction.setDescription(desc);
                transaction.setCategoryId(selectedCategoryId);
                Database.insertTransaction(transaction);

                Intent returnIntent = new Intent(activity, HomeActivity.class);
                returnIntent.putExtra(IntentKeys.PAGE, ApplicationPage.LAST_ACCOUNT);
                activity.setResult(Activity.RESULT_OK, returnIntent);
                activity.finish();
            }
        }
    }

    private void onModify(View view) {
        if (activity != null) {
            String newDesc = ((EditText) view.findViewById(R.id.transaction_desc_editText)).getText().toString();
            int newValue = BalanceUtils.parseBalanceValue(((EditText) view.findViewById(R.id.transaction_value_editText)).getText().toString());
            Account account = Database.findAccount(transaction.getAccountId());
            if (account != null && account.getAccountBalance() + newValue < 0 && !canAccountBeNegative(transaction.getAccountId(), activity)) {
                showNegativityDialog(activity, BalanceUtils.balanceStringFromInt(-(account.getAccountBalance() + transaction.getCentValue())), account.getAccountName());
            } else {
                if (!transaction.getDescription().equals(newDesc) || transaction.getCentValue() != newValue || transaction.getCategoryId() != selectedCategoryId) {
                    if (!transaction.getDescription().equals(newDesc)) {
                        transaction.setDescription(newDesc);
                    }
                    if (transaction.getCentValue() != newValue) {
                        int diff = newValue - transaction.getCentValue();
                        if (account != null) {
                            account.setAccountBalance(account.getAccountBalance() + diff);
                        }
                        transaction.setCentValue(newValue);
                    }

                    if (transaction.getCategoryId() != selectedCategoryId && transaction.getCategoryId() != 2) {
                        transaction.setCategoryId(selectedCategoryId);
                    }
                    Database.updateTransaction(transaction);
                }
                Intent returnIntent = new Intent(activity, HomeActivity.class);
                returnIntent.putExtra(IntentKeys.PAGE, ApplicationPage.LAST_ACCOUNT);
                activity.setResult(Activity.RESULT_OK, returnIntent);
                activity.finish();
            }
        }
    }

    private boolean canAccountBeNegative(long accountId, Context context) {
        return context.getSharedPreferences(SharedPreferencesKeys.ACCOUNT_PREFIX + accountId, Context.MODE_PRIVATE)
                      .getBoolean(SharedPreferencesKeys.CAN_ACCOUNT_BE_NEGATIVE, true);
    }

    private void showNegativityDialog(Context context, String maxPossibleValue, String accountName) {
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setMessage(getString(R.string.alert_dialog_account_negative_message, maxPossibleValue, accountName))
                .setTitle(R.string.alert_dialog_title)
                .setNegativeButton(R.string.alert_dialog_dismiss_text, (dialog1, which) -> dialog1.dismiss())
                .create();
        dialog.show();
    }
}
