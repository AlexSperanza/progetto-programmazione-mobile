package progettomobile.balancemanager.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.dialogs.SelectionDialog;
import com.google.android.material.tabs.TabLayout;

public class CurrentAccountFragment extends Fragment implements AddButtonListener {

    private Activity activity;

    public CurrentAccountFragment() {

    }

    public CurrentAccountFragment(@NonNull Activity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (container != null) {
            container.removeAllViews();
        }
        View view = inflater.inflate(R.layout.tabbed_current_account_fragment , container, false);
        if (activity == null && getActivity() != null) {
            activity = getActivity();
        }
        if (activity != null) {
            getChildFragmentManager().beginTransaction().replace(R.id.tab_layout_fragment, new CurrentAccountSummaryFragment(activity)).commit();
            TabLayout tabLayout = view.findViewById(R.id.tabLayout);
            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    int position = tab.getPosition();
                    if (position == 0) { // Summary tab
                        getChildFragmentManager().beginTransaction().replace(R.id.tab_layout_fragment, new CurrentAccountSummaryFragment(activity)).commit();
                    } else { // History tab
                        getChildFragmentManager().beginTransaction().replace(R.id.tab_layout_fragment, new CurrentAccountHistoryFragment(activity)).commit();
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

        }
        return view;
    }

    @Override
    public View.OnClickListener getAddButtonListener() {
        return v -> {
            if (activity != null) {
                SelectionDialog dialog = new SelectionDialog(activity);
                dialog.show();
            }
        };
    }
}
