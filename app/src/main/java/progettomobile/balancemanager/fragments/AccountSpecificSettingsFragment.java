package progettomobile.balancemanager.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.constants.SharedPreferencesKeys;
import progettomobile.balancemanager.database.Database;
import progettomobile.balancemanager.database.entities.Account;

public class AccountSpecificSettingsFragment extends Fragment {

    private @NonNull Activity activity;
    private long accountId;

    public AccountSpecificSettingsFragment(@NonNull Activity activity, long accountId) {
        this.activity = activity;
        this.accountId = accountId;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (container != null) {
            container.removeAllViews();
        }
        View view = inflater.inflate(R.layout.account_specific_settings_fragment, container, false);
        initSwitch(view);

        return view;
    }

    private void initSwitch(View view) {
        Switch switchView = view.findViewById(R.id.account_negativity_switch);
        Account account = Database.findAccount(accountId);
        if (account != null && account.getAccountBalance() < 0) {
            switchView.setOnClickListener(v -> {
                switchView.setChecked(false);
                new AlertDialog.Builder(activity)
                        .setTitle(R.string.alert_dialog_title)
                        .setMessage(R.string.alert_dialog_account_balance_negative_setting)
                        .setNegativeButton(R.string.alert_dialog_dismiss_text, (dialog, which) -> dialog.dismiss())
                        .create()
                        .show();
            });
        } else {
            SharedPreferences accountSpecificPreferences = activity.getSharedPreferences(SharedPreferencesKeys.ACCOUNT_PREFIX + accountId, Activity.MODE_PRIVATE);
            switchView.setChecked(!accountSpecificPreferences.getBoolean(SharedPreferencesKeys.CAN_ACCOUNT_BE_NEGATIVE, true));
            switchView.setOnCheckedChangeListener((buttonView, isChecked) -> accountSpecificPreferences.edit().putBoolean(SharedPreferencesKeys.CAN_ACCOUNT_BE_NEGATIVE, !isChecked).apply());
        }
    }
}
