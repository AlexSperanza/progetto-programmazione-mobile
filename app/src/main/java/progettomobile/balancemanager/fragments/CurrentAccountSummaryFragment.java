package progettomobile.balancemanager.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.activities.HomeActivity;
import progettomobile.balancemanager.constants.ApplicationPage;
import progettomobile.balancemanager.constants.IntentKeys;
import progettomobile.balancemanager.constants.SharedPreferencesKeys;
import progettomobile.balancemanager.database.Database;
import progettomobile.balancemanager.database.entities.Account;
import progettomobile.balancemanager.utils.BalanceUtils;

public class CurrentAccountSummaryFragment extends Fragment {

    private Activity activity;

    public CurrentAccountSummaryFragment() {}

    public CurrentAccountSummaryFragment(@NonNull Activity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (container != null) {
            container.removeAllViews();
        }
        View view = inflater.inflate(R.layout.current_account_summary_tab_fragment, container, false);
        if (activity == null && getActivity() != null) {
            activity = getActivity();
        }

        if (activity != null) {
            long currentAccountId = activity.getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE)
                                            .getLong(SharedPreferencesKeys.LAST_ACCOUNT, SharedPreferencesKeys.NO_CURRENT_ACCOUNT);
            Account currentAccount = Database.findAccount(currentAccountId);
            if (currentAccount != null) {
                ((TextView) view.findViewById(R.id.report_value_textview))
                        .setText(getString(R.string.balance_with_currency,
                                           BalanceUtils.balanceStringFromInt(currentAccount.getAccountBalance()),
                                           activity.getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE)
                                                   .getString(SharedPreferencesKeys.CURRENCY_PREFERENCE, "€")));
            }
            view.findViewById(R.id.accountListShortcutButton).setOnClickListener(v -> {
                Intent intent = new Intent(activity, HomeActivity.class);
                intent.putExtra(IntentKeys.PAGE, ApplicationPage.ACCOUNT_LIST);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            });
            view.findViewById(R.id.categoryListShortcutButton).setOnClickListener(v -> {
                Intent intent = new Intent(activity, HomeActivity.class);
                intent.putExtra(IntentKeys.PAGE, ApplicationPage.CATEGORY_LIST);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            });
        }
        return view;
    }
}
