package progettomobile.balancemanager.calendarview.utils;

import progettomobile.balancemanager.calendarview.basics.Date;
import progettomobile.balancemanager.calendarview.basics.DateMonthType;

import org.joda.time.LocalDate;
import org.joda.time.Weeks;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CalendarUtil {

    public static Date getDate(LocalDate localDate, DateMonthType type) {
        int year = localDate.getYear(), month = localDate.getMonthOfYear(), day = localDate.getDayOfMonth();
        Date date = new Date(year, month, day);
        date.setWeek(localDate.getDayOfWeek());
        date.setType(type);
        return date;
    }

    public static List<List<Date>> getMonthOfWeekDates(int year, int month) {
//        long curTime = System.currentTimeMillis();
        LocalDate localDate = new LocalDate(year, month, 1);
        List<List<Date>> weeks = new ArrayList<>();
        while (localDate.getMonthOfYear() <= month && localDate.getYear() == year) {
            weeks.add(getWeekDates(localDate.getYear(), localDate.getMonthOfYear(), localDate.getDayOfMonth()));
            localDate = localDate.plusWeeks(1).withDayOfWeek(1);

        }
        return weeks;
    }

    public static List<Date> getMonthDates(int year, int month) {
//        long curTime = System.currentTimeMillis();
        LocalDate localDate = new LocalDate(year, month, 1);
        List<Date> dates = new ArrayList<>();
        while (localDate.getMonthOfYear() <= month && localDate.getYear() == year) {
            dates.addAll(getWeekDates(localDate.getYear(), localDate.getMonthOfYear(), localDate.getDayOfMonth()));
            localDate = localDate.plusWeeks(1).withDayOfWeek(1);
        }
        return dates;
    }


    public static List<Date> getWeekDates(int year, int month, int day) {
        List<Date> dates = new ArrayList<>();
        LocalDate localDate = new LocalDate(year, month, day);
        for (int i = 1; i <= 7; i++) {
            LocalDate tempDate = localDate.withDayOfWeek(i);
            int tempMonth = tempDate.getMonthOfYear();
            DateMonthType type;
            if (tempMonth == month) {
                type = DateMonthType.THIS_MONTH;
            } else if (tempMonth > month) {
                type = DateMonthType.NEXT_MONTH;
            } else {
                type = DateMonthType.LAST_MONTH;
            }
            dates.add(getDate(tempDate, type));

        }
        return dates;
    }

    public static int getDayForWeek(int y, int m, int d) {
        Calendar calendar = Calendar.getInstance();
        //Months start from 0
        calendar.set(y, m -1, d);
        return calendar.get(Calendar.DAY_OF_WEEK);
    }

    /**
     * Get date from position.
     * @param position  starting month
     */
    public static int[] positionToDate(int position, int startY, int startM) {
        int year = position / 12 + startY;
        int month = position % 12 + startM;

        if (month > 12) {
            month = month % 12;
            year = year + 1;
        }
        return new int[]{year, month};
    }

    /**
     * Get month difference between 2 dates
     * @return number of month
     */
    public static int getMonthPosition(int year1, int month1, int year2, int month2) {
        int year = year2 - year1;
        int month = month2 - month1;

        return year * 12 + month;
    }

    /**
     * Get week difference between two dates
     * @param startYear     first date year
     * @param startMonth    first date month
     * @return  week difference
     */
    public static int getWeekPosition(int startYear, int startMonth, int startDay, int year, int month, int day) {
        LocalDate start = new LocalDate(startYear, startMonth, startDay).withDayOfWeek(1);
        LocalDate end = new LocalDate(year, month, day).withDayOfWeek(1);
        return Weeks.weeksBetween(start, end).getWeeks();
    }

    /**
     * Get the day of the week for a given week
     * @param startYear     Starting year
     * @param startMonth    Starting Month
     * @param positionWeek  Offset Week
     * @return  the days of the week, the first is Sunday
     */
    public static List<Date> getWeekDaysForPosition(int startYear, int startMonth, int startDay, int positionWeek) {
        ArrayList<Date> dates = new ArrayList<>();

        LocalDate localDate = new LocalDate(startYear, startMonth, startDay).plusWeeks(positionWeek);

        for (int i = 1; i <= 7; i++) {
            dates.add(getDate(localDate.withDayOfWeek(i), DateMonthType.THIS_MONTH));
        }
        return dates;
    }

    public static LocalDate getCurrentDate() {
        return new LocalDate();
    }

    public static int getMaxDayByYearMonth(int year,int month) {
        int maxDay;
        int day = 1;

        Calendar calendar = Calendar.getInstance();

        calendar.set(year,month - 1,day);

        maxDay = calendar.getActualMaximum(Calendar.DATE);
        return maxDay;
    }
}
