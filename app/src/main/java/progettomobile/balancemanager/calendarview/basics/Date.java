package progettomobile.balancemanager.calendarview.basics;

import androidx.annotation.NonNull;

import org.joda.time.LocalDate;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Calendar;

public class Date {

	private int year;
	private int month;
	private int day;
	private int week;
	private DateMonthType type;

	private boolean scheme;

	public Date() {
	}

	public Date(int year, int month, int day) {
		this.year = year;
		this.month = month;
		this.day = day;
		type = DateMonthType.THIS_MONTH;
	}

	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getWeek() {
		return week;
	}

	public void setWeek(int week) {
		this.week = week;
	}

	public DateMonthType getType() {
		return type;
	}

	public void setType(DateMonthType type) {
		this.type = type;
	}

    public String getDate(DateFormat dateFormat) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month -1, day);
		return dateFormat.format(calendar.getTime());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (obj instanceof Date) {
			Date other = (Date) obj;
			if (other.year == year && other.month == month && other.day == day && other.type == type) {
				return true;
			}
		}
		return false;
	}


	public boolean isBefore(@NonNull Date other) {
		if (year == other.year) {
			return ((month == other.month) ? (day < other.day) : (month < other.month));
		} else {
			return year < other.year;
		}
	}

	public boolean isAfter(@NonNull Date other) {

		if (year == other.year) {
			return (month == other.month) ? (day > other.day) : (month > other.month);
		} else {
			return year > other.year;
		}
	}

    public boolean isScheme() {
        return scheme;
    }

    public void setScheme(boolean scheme) {
        this.scheme = scheme;
    }

    @NonNull
	@Override
	public String toString() {
		DecimalFormat decimalFormat = new DecimalFormat();
		decimalFormat.applyPattern("00");
		return year + "-" + decimalFormat.format(month) + "-" + decimalFormat.format(day);
	}

	@NonNull
	public LocalDate toJodaDate() {
		return LocalDate.parse(this.toString());
	}

}
