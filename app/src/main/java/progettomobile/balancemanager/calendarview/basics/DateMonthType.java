package progettomobile.balancemanager.calendarview.basics;

public enum DateMonthType {
    LAST_MONTH,
    THIS_MONTH,
    NEXT_MONTH;
}

