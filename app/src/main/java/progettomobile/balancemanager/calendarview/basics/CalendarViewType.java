package progettomobile.balancemanager.calendarview.basics;

public enum CalendarViewType {
    MONTH,
    WEEK;
}