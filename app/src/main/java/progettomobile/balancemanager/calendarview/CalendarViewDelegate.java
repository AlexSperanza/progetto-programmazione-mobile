package progettomobile.balancemanager.calendarview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;

import androidx.annotation.ColorInt;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.calendarview.basics.Date;
import progettomobile.balancemanager.calendarview.listeners.OnDateSelectedListener;
import progettomobile.balancemanager.calendarview.utils.DensityUtil;
import progettomobile.balancemanager.database.Database;
import progettomobile.balancemanager.database.relations.TransactionWithCategory;

import org.joda.time.LocalDate;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

public class CalendarViewDelegate {
    private int mTopTextSize;                              //Calendar date font size
    private int mBottomTextSize;                            //Bottom text font
    private @ColorInt int mTopTextColor;        //Date color
    private @ColorInt int mBottomTextColor;   //Bottom text color
    private @ColorInt int mBackgroundColor;    //Background color
    private @ColorInt int mSelectedItemColor;
    private @ColorInt int mSelectedTextColor;
    private OnDateSelectedListener mOnInnerDateSelectedListener;
    private @ColorInt int mSchemeColor;
    private int mSchemeRadius;
    private @ColorInt int mSelectedSchemeColor;
    private @ColorInt int mWeekInfoBackgroundColor;
    private @ColorInt int mWeekInfoTextColor;
    private int mWeekInfoTextSize;
    private Date mSelectedDate;
    private int startYear, startMonth;

    public CalendarViewDelegate(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CalendarView);
        startYear = a.getInt(R.styleable.CalendarView_startYear, 1980);
        startMonth = a.getInt(R.styleable.CalendarView_startMonth, 1);
        mTopTextSize = a.getDimensionPixelSize(R.styleable.CalendarView_topTextSize,
                DensityUtil.sp2px(context, 16));
        mTopTextColor = a.getColor(R.styleable.CalendarView_topTextColor, Color.BLACK);

        mBottomTextSize = a.getDimensionPixelSize(R.styleable.CalendarView_bottomTextSize,
                DensityUtil.sp2px(context, 10));
        mBottomTextColor = a.getColor(R.styleable.CalendarView_bottomTextColor, Color.parseColor("#999999"));
        mSelectedItemColor = a.getColor(R.styleable.CalendarView_selectedItemColor, Color.GRAY);
        mSelectedTextColor = a.getColor(R.styleable.CalendarView_selectedItemTextColor, Color.WHITE);

        mSchemeRadius = a.getDimensionPixelSize(R.styleable.CalendarView_schemeRadius,
                DensityUtil.dp2px(context, 2));
        mSchemeColor = a.getColor(R.styleable.CalendarView_schemeColor, mSelectedItemColor);
        mSelectedSchemeColor = a.getColor(R.styleable.CalendarView_selectedSchemeColor, mSelectedTextColor);

        mBackgroundColor = a.getColor(R.styleable.CalendarView_monthBackgroundColor, Color.WHITE);

        mWeekInfoBackgroundColor = a.getColor(R.styleable.CalendarView_weekInfoBackgroundColor, Color.WHITE);
        mWeekInfoTextColor = a.getColor(R.styleable.CalendarView_weekInfoTextColor, Color.BLACK);
        mWeekInfoTextSize = a.getDimensionPixelSize(R.styleable.CalendarView_weekInfoTextSize,
                DensityUtil.sp2px(context, 16));
        a.recycle();
    }

    public int getTopTextSize() {
        return mTopTextSize;
    }

    public void setTopTextSize(int topTextSize) {
        mTopTextSize = topTextSize;
    }

    public int getBottomTextSize() {
        return mBottomTextSize;
    }

    public void setBottomTextSize(int bottomTextSize) {
        mBottomTextSize = bottomTextSize;
    }

    public int getTopTextColor() {
        return mTopTextColor;
    }

    public void setTopTextColor(int topTextColor) {
        mTopTextColor = topTextColor;
    }

    public int getBottomTextColor() {
        return mBottomTextColor;
    }

    public void setBottomTextColor(int bottomTextColor) {
        mBottomTextColor = bottomTextColor;
    }

    public int getBackgroundColor() {
        return mBackgroundColor;
    }

    public void setBackgroundColor(int backgroundColor) {
        mBackgroundColor = backgroundColor;
    }

    public int getSelectedItemColor() {
        return mSelectedItemColor;
    }

    public void setSelectedItemColor(int selectedItemColor) {
        mSelectedItemColor = selectedItemColor;
    }

    public int getSelectedTextColor() {
        return mSelectedTextColor;
    }

    public void setSelectedTextColor(int selectedTextColor) {
        mSelectedTextColor = selectedTextColor;
    }

    public OnDateSelectedListener getOnInnerDateSelectedListener() {
        return mOnInnerDateSelectedListener;
    }

    public void setOnInnerDateSelectedListener(OnDateSelectedListener onInnerDateSelectedListener) {
        mOnInnerDateSelectedListener = onInnerDateSelectedListener;
    }

    public int getSchemeColor() {
        return mSchemeColor;
    }

    public void setSchemeColor(int schemeColor) {
        mSchemeColor = schemeColor;
    }

    public int getSchemeRadius() {
        return mSchemeRadius;
    }

    public void setSchemeRadius(int schemeRadius) {
        mSchemeRadius = schemeRadius;
    }

    public int getSelectedSchemeColor() {
        return mSelectedSchemeColor;
    }

    public void setSelectedSchemeColor(int selectedSchemeColor) {
        mSelectedSchemeColor = selectedSchemeColor;
    }

    public int getWeekInfoBackgroundColor() {
        return mWeekInfoBackgroundColor;
    }

    public void setWeekInfoBackgroundColor(int weekInfoBackgroundColor) {
        mWeekInfoBackgroundColor = weekInfoBackgroundColor;
    }

    public int getWeekInfoTextColor() {
        return mWeekInfoTextColor;
    }

    public void setWeekInfoTextColor(int weekInfoTextColor) {
        mWeekInfoTextColor = weekInfoTextColor;
    }

    public int getWeekInfoTextSize() {
        return mWeekInfoTextSize;
    }

    public void setWeekInfoTextSize(int weekInfoTextSize) {
        mWeekInfoTextSize = weekInfoTextSize;
    }

    public Date getSelectedDate() {
        return mSelectedDate;
    }

    public void setSelectedDate(Date selectedDate) {
        mSelectedDate = selectedDate;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public String getSelectedMonth() {
        if (mSelectedDate != null) {
            return mSelectedDate.getDate(new SimpleDateFormat("MMMM"));
        } else {
            return "";
        }
    }

    public String getSelectedYear() {
        if (mSelectedDate != null) {
            return String.valueOf(mSelectedDate.getYear());
        } else {
            return "";
        }
    }

    public String getBottomText(LocalDate date) {
        Map<LocalDate, List<TransactionWithCategory>> accountTransactions = Database.getCurrentAccountTransactions();
        if (accountTransactions.containsKey(date)) {
            List<TransactionWithCategory> transactionList = accountTransactions.get(date);
            String bottomString = "";
            for (int i = 0; i < transactionList.size() && bottomString.length() < 1; i++) {
                if (transactionList.get(i).getTransaction().getCentValue() > 0) {
                    bottomString += "▲";
                }
            }
            for (int i = 0; i < transactionList.size() && bottomString.length() < 2; i++) {
                if (transactionList.get(i).getTransaction().getCentValue() < 0) {
                    bottomString += "▼";
                }
            }
            if (bottomString.isEmpty() && !transactionList.isEmpty()) {
                bottomString = "~";
            }
            return bottomString;
        } else {
            return "";
        }
    }
}
