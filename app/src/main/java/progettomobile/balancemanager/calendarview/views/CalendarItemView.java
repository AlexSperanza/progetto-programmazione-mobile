package progettomobile.balancemanager.calendarview.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

import androidx.annotation.NonNull;

import progettomobile.balancemanager.calendarview.CalendarViewDelegate;
import progettomobile.balancemanager.calendarview.basics.Date;
import progettomobile.balancemanager.calendarview.basics.DateMonthType;

import java.util.List;

@SuppressLint("ViewConstructor")
public class CalendarItemView extends View {

    /* There cannot be more than 6 rows as a month can start in the middle of a week and has got 31
    *  days, so its last weekdays are in the sixth rows: 31 / 6 = 5,167 ceiled is 6*/
    public static final int MAX_ROW = 6;

    /* Columns are the week days*/
    private static final int MAX_COLUMN = 7;
    private Paint mPaint;
    private List<Date> mDates;
    private CalendarViewDelegate mCalendarViewDelegate;

    //Number of rows displayed after setting Data
    private int mRow;
    private int mItemWidth, mItemHeight;
    private int mFirstItemDrawX, mFirstItemDrawY;

    /**
     * Size of control drawing
     */
    private Rect mDrawnRect;
    private int mTouchSlop;

    /**
     * The theoretical maximum number of rows in a month of the calendar is 6,
     * when the number of rows in the current month is less than 6,
     * in order to make the height more overall,
     * the more parts are divided equally into each interval.
     */
    private int mItemHeightSpace;
    private float mTouchDownX, mTouchDownY;
    private int mSelectedItemPosition = -1;
    private List<Date> mSchemes;

    public CalendarItemView(Context context, @NonNull CalendarViewDelegate calendarViewDelegate) {
        super(context);
        mCalendarViewDelegate = calendarViewDelegate;
        initPaint();
        mDrawnRect = new Rect();
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
        super.setBackgroundColor(mCalendarViewDelegate.getBackgroundColor());
    }

    public void setDateList(List<Date> dates) {
        if (dates == null || dates.size() == 0 || dates.size() % MAX_COLUMN != 0) {
            throw new IllegalArgumentException("dates must be divisible by" + MAX_COLUMN);
        }
        mSelectedItemPosition = -1;
        //check whether the selected date is in the same month of the previous date
        if (dates.size() > MAX_COLUMN) { //The size of the weekly mode dates cannot be greater than MAX_COLUMN
            Date date = dates.get(MAX_COLUMN - 1);
            Date selectedDate = mCalendarViewDelegate.getSelectedDate();
            if (selectedDate != null &&selectedDate.getYear() == date.getYear() &&
                    selectedDate.getMonth() == date.getMonth()) {
                mSelectedItemPosition = dates.indexOf(selectedDate);
            }
        }
        mDates = dates;
        mRow = mDates.size() / MAX_COLUMN;
        mSchemes = null;
        requestLayout();
    }

    public List<Date> getDates() {
        return mDates;
    }

    /**
     * Determine whether the date is in the current view
     * @param date date to check
     * @return position if found, -1 otherwise
     */
    public int indexThisView(Date date) {
        if (mDates == null || date == null) {
            return -1;
        }
        if (mDates.size() <= 7) {
            return mDates.indexOf(date);

        } else { // mDates.size() > 7
            //15 is to get a date which is certainly in the current month
            Date month = mDates.get(15);
            if (month.getYear() == date.getYear() && month.getMonth() == date.getMonth()) {
                return mDates.indexOf(date);
            }
        }
        return -1;
    }

    public void setScheme(List<Date> schemes) {
        if (mSchemes == schemes) {
            return;
        }
        for (Date date : mDates) {
            if (schemes != null && schemes.indexOf(date) >= 0) {
                date.setScheme(true);
            } else {
                date.setScheme(false);
            }
        }
        postInvalidate();
    }

    public void selectDate(Date date) {
        int position = mDates.indexOf(date);
        if (position >=0) {
            selectDate(position);
        } else {
            cancelSelected();
        }
    }

    public void selectDate(int position) {
        if (position >= 0 && position < mDates.size()) {
            if (mSelectedItemPosition != -1 && mCalendarViewDelegate.getSelectedDate() == getDates().get(mSelectedItemPosition)) {
                return;
            }
            mSelectedItemPosition = position;
            postInvalidate();
            if (mCalendarViewDelegate.getOnInnerDateSelectedListener() != null) {
                mCalendarViewDelegate.getOnInnerDateSelectedListener()
                        .onDateSelected((mDates.get(position)));
            }
        }
    }

    public void cancelSelected() {
        if (mSelectedItemPosition > -1) {
            mSelectedItemPosition = -1;
            postInvalidate();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mTouchDownX = event.getX();
                mTouchDownY = event.getY();
                return true;
            case MotionEvent.ACTION_UP:
                float disX = mTouchDownX - event.getX();
                float disY = mTouchDownY - event.getY();
                if (Math.abs(disX) < mTouchSlop && Math.abs(disY) < mTouchSlop) {
                    if (mDrawnRect.contains((int) event.getX(), (int) event.getY())) {
                        int row = (int) (event.getY() / (mItemHeight + mItemHeightSpace));
                        int column = (int) (event.getX() / mItemWidth);
                        int position = row * MAX_COLUMN + column;

                        selectDate(position);
                        return true;
                    }
                }
                break;
        }
        return super.onTouchEvent(event);
    }
    public int getItemHeight() {
        return mItemHeight;
    }

    public int getSelectedItemTop() {
        int row = mSelectedItemPosition / MAX_COLUMN;
        return row * (mItemHeight + mItemHeightSpace);
    }
    public int getSelectedItemBottom() {
        return getSelectedItemTop() + mItemHeight;
    }
    public int getMaxHeight() {
        return mItemHeight * MAX_ROW;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthSpecSize = MeasureSpec.getSize(widthMeasureSpec);

        int itemWidth = widthSpecSize / MAX_COLUMN;

        int height;
        if (mRow == 1) {
            height = itemWidth;
        } else {
            height = itemWidth * MAX_ROW;
        }
        setMeasuredDimension(widthMeasureSpec, height);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mDrawnRect.set(getPaddingLeft(), getPaddingTop(),
                getWidth() - getPaddingRight(), getHeight() - getPaddingBottom());
        mItemWidth = mDrawnRect.width() / MAX_COLUMN;
        if (mRow == 1) {
            mItemHeight = mDrawnRect.height();
        } else {
            mItemHeight = mDrawnRect.height() / MAX_ROW;
        }

        mFirstItemDrawX = mItemWidth / 2;
        mFirstItemDrawY = (int) ((mItemHeight - (mPaint.ascent() + mPaint.descent())) / 2);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mItemHeightSpace = (MAX_ROW - mRow) * mItemHeight / mRow;
        if (mSelectedItemPosition >= 0 && mSelectedItemPosition < mDates.size()) {
            int row = mSelectedItemPosition / MAX_COLUMN;
            int column = mSelectedItemPosition % MAX_COLUMN;
            int top = row * (mItemHeight + mItemHeightSpace);
            int drawX = column * mItemWidth + mItemWidth / 2;
            int drawY = top + mItemHeight / 2;
            mPaint.setColor(mCalendarViewDelegate.getSelectedItemColor());
            canvas.drawCircle(drawX, drawY, mItemWidth / 2, mPaint);
        }
        mPaint.setTextAlign(Paint.Align.CENTER);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setTextSize(mCalendarViewDelegate.getTopTextSize());
        for (int i = 0; i < mDates.size(); i++) {
            int row = i / MAX_COLUMN;
            int column = i % MAX_COLUMN;
            Date date = mDates.get(i);
            mPaint.setTextSize(mCalendarViewDelegate.getTopTextSize());
            if (i == mSelectedItemPosition) {
                //The current item is selected, paint the text with the color
                mPaint.setColor(mCalendarViewDelegate.getSelectedTextColor());
            } else if (date.getType() == DateMonthType.THIS_MONTH || mRow == 1) { //TYPE_THIS_MONTH is for the days of the current month; mRow==1 means the view is in weekview mode
                mPaint.setColor(mCalendarViewDelegate.getTopTextColor());
            } else {
                //The style of dates not in the current month is Bottom
                mPaint.setColor(mCalendarViewDelegate.getBottomTextColor());
            }
            int itemDrawX = mFirstItemDrawX + column * mItemWidth;
            int itemDrawY = mFirstItemDrawY + row * (mItemHeight + mItemHeightSpace);
            canvas.drawText(date.getDay() + "", itemDrawX, itemDrawY, mPaint);

            mPaint.setTextSize(20);
            String text = mCalendarViewDelegate.getBottomText(date.toJodaDate());
            if (!text.isEmpty()) {
                float bottomDrawY = mFirstItemDrawY * 1.4f + row * (mItemHeight + mItemHeightSpace);
                canvas.drawText(text, itemDrawX, bottomDrawY, mPaint);
            }
        }
    }

    private void initPaint() {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG | Paint.LINEAR_TEXT_FLAG);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setTextAlign(Paint.Align.CENTER);
        mPaint.setTextSize(mCalendarViewDelegate.getTopTextSize());
    }

    /**
     *  Calculate size
     * @param specMode Measurement mode
     * @param specSize measured size
     * @param size     Required size
     * @return actual size
     */
    private int measureSize(int specMode, int specSize, int size) {
        if (specMode == MeasureSpec.EXACTLY) {
            return specSize;
        } else {
            return Math.min(specSize, size);
        }
    }
}
