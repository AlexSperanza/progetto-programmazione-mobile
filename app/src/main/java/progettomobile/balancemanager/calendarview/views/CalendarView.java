package progettomobile.balancemanager.calendarview.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;


import progettomobile.balancemanager.calendarview.CalendarViewDelegate;
import progettomobile.balancemanager.calendarview.adapters.MonthCalendarAdapter;
import progettomobile.balancemanager.calendarview.adapters.WeekCalendarAdapter;
import progettomobile.balancemanager.calendarview.basics.CalendarViewType;
import progettomobile.balancemanager.calendarview.basics.Date;
import progettomobile.balancemanager.calendarview.basics.DateMonthType;
import progettomobile.balancemanager.calendarview.listeners.OnDateSelectedListener;
import progettomobile.balancemanager.calendarview.listeners.OnPageSelectedListener;
import progettomobile.balancemanager.calendarview.pagers.CalendarItemPager;
import progettomobile.balancemanager.calendarview.pagers.PagerInfo;
import progettomobile.balancemanager.calendarview.utils.CalendarUtil;

import org.joda.time.LocalDate;

import java.util.List;

@SuppressWarnings("unused")
public class CalendarView extends ViewGroup {

    private CurrentMonthView currentMonthLabel;
    private CalendarItemPager mWeekPager, mMonthPager;
    private CalendarViewDelegate mCalendarViewDelegate;
    private WeekInfoView mWeekInfoView;
    private CalendarViewType mCalendarType = CalendarViewType.MONTH;

    private OnDateSelectedListener mOnDateSelectedListener;
    private OnPageSelectedListener mOnPageSelectedListener;

    private float touchEventX1;
    private float touchEventY1;

    public CalendarView(Context context) {
        this(context, null);
    }

    public CalendarView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CalendarView(final Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mCalendarViewDelegate = new CalendarViewDelegate(context, attrs);
        mCalendarViewDelegate.setOnInnerDateSelectedListener(date -> {
            mCalendarViewDelegate.setSelectedDate(date);
            if (mCalendarType == CalendarViewType.WEEK) {
                scrollMonthToDate(date.getYear(), date.getMonth(), date.getDay(), false);
            } else {
                //If selected on month mode, jump to current date
                if (date.getType() != DateMonthType.THIS_MONTH) {
                    scrollMonthToDate(date.getYear(), date.getMonth(), date.getDay(), true);
                    return;
                }
                scrollWeekToDate(date.getYear(), date.getMonth(), date.getDay(), false);
            }
            if (mOnDateSelectedListener != null) {
                mOnDateSelectedListener.onDateSelected(date);
            }
            CalendarView.this.updateCalendar();
        });

        initChild();
        setDateToCurrent();
        mWeekPager.setVisibility(INVISIBLE);
        mMonthPager.setVisibility(VISIBLE);

    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                return onTouchEvent(event);
        }
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touchEventX1 = event.getX();
                touchEventY1 = event.getY();
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                float x2 = event.getX();
                float y2 = event.getY();
                if (Math.abs(x2 - touchEventX1) < getMeasuredWidth() / 7.0) { // the swipe is not horizontal
                    float minVerticalDistance = getMeasuredHeight() / 3f;
                    if (y2 - touchEventY1 < -minVerticalDistance && getCalendarType() == CalendarViewType.MONTH) {
                        setTypeToWeek();
                        return true;
                    }
                    if (y2 - touchEventY1 > minVerticalDistance && getCalendarType() == CalendarViewType.WEEK) {
                        setTypeToMonth();
                        return true;
                    }
                }
                break;
        }
        return false;
    }

    public CalendarItemPager getWeekPager() {
        return mWeekPager;
    }

    public CalendarItemPager getMonthPager() {
        return mMonthPager;
    }

    public CalendarViewDelegate getCalendarViewDelegate() {
        return mCalendarViewDelegate;
    }

    public void scrollToSelectedDate() {
        scrollToDate(mCalendarViewDelegate.getSelectedDate(), false);
    }

    public void scrollToDate(Date date, boolean smoothScroll) {
        scrollToDate(date.getYear(), date.getMonth(), date.getDay(), smoothScroll);
    }

    public void scrollWeekToDate(int year, int month, int day, boolean smoothScroll) {
        int position = CalendarUtil.getWeekPosition(mCalendarViewDelegate.getStartYear(),
                mCalendarViewDelegate.getStartMonth(), 1,
                year, month, day);
        mWeekPager.setCurrentItem(position, smoothScroll);
        CalendarItemView calendarItemView = getWeekCurrentItem();
        if (calendarItemView != null) {
            calendarItemView.selectDate(new Date(year, month, day));
        }
    }
    public void scrollMonthToDate(int year, int month, int day, boolean smoothScroll) {
        int position = CalendarUtil.getMonthPosition(mCalendarViewDelegate.getStartYear(),
                mCalendarViewDelegate.getStartMonth(), year, month);
        mMonthPager.setCurrentItem(position, smoothScroll);
        CalendarItemView calendarItemView = getMonthCurrentItem();
        if (calendarItemView != null) {
            calendarItemView.selectDate(new Date(year, month, day));
        }
    }

    @Nullable
    public CalendarItemView getCurrentCalendarItemView() {
        CalendarItemView calendarItemView;
        if (mCalendarType == CalendarViewType.MONTH) {
            calendarItemView = getMonthCurrentItem();
        } else {
            calendarItemView = getWeekCurrentItem();
        }
        return calendarItemView;
    }

    public CalendarViewType getCalendarType() {
        return mCalendarType;
    }

    public void setCalendarType(CalendarViewType calendarType) {
        if (calendarType == CalendarViewType.MONTH) {
            setTypeToMonth();
        } else {
            setTypeToWeek();
        }
    }

    public void setTypeToWeek() {
        selectedWeekPage();
        if (mCalendarType == CalendarViewType.WEEK) {
            return;
        }
        mCalendarType = CalendarViewType.WEEK;
        mMonthPager.setTranslationY(0);
        mMonthPager.setVisibility(GONE);
        mWeekPager.setVisibility(VISIBLE);
        scrollToSelectedDate();

    }

    public void setTypeToMonth() {
        selectedMonthPage();

        if (mCalendarType == CalendarViewType.MONTH) {
            return;
        }
        mCalendarType = CalendarViewType.MONTH;
        mMonthPager.setTranslationY(0);
        mMonthPager.setVisibility(VISIBLE);
        mWeekPager.setVisibility(GONE);
        scrollToSelectedDate();
    }

    public void setSchemesToCurrentView(List<Date> schemes) {
        CalendarItemView calendarItemView = getCurrentCalendarItemView();
        if (calendarItemView != null) {
            calendarItemView.setScheme(schemes);
        }
    }

    public void scrollToDate(int year, int month, int day, boolean smoothScroll) {
        if (mMonthPager.getVisibility() == VISIBLE) {
            scrollMonthToDate(year, month, day, smoothScroll);
            scrollWeekToDate(year, month, day, false);
        } else {
            scrollMonthToDate(year, month, day, false);
            scrollWeekToDate(year, month, day, smoothScroll);
        }
    }

    public void setTopTextSize(int textSize) {
        mCalendarViewDelegate.setTopTextSize(textSize);
        updateCalendar();
    }

    public void setTopTextColor(@ColorInt int color) {
        mCalendarViewDelegate.setTopTextColor(color);
        updateCalendar();
    }

    public void setBottomTextSize(int textSize) {
        mCalendarViewDelegate.setBottomTextSize(textSize);
        updateCalendar();
    }

    public void setBottomTextColor(@ColorInt int color) {
        mCalendarViewDelegate.setBottomTextColor(color);
        updateCalendar();
    }

    public void setSelectedItemColor(@ColorInt int color) {
        mCalendarViewDelegate.setSelectedItemColor(color);
        updateCalendar();
    }

    public void setSelectedTextColor(@ColorInt int color) {
        mCalendarViewDelegate.setSelectedTextColor(color);
        updateCalendar();
    }

    public void setSchemeColor(@ColorInt int color) {
        mCalendarViewDelegate.setSchemeColor(color);
        updateCalendar();
    }

    public void setSchemeRadius(int radius) {
        mCalendarViewDelegate.setSchemeRadius(radius);
        updateCalendar();
    }

    public void setSelectedSchemeColor(@ColorInt int color) {
        mCalendarViewDelegate.setSelectedSchemeColor(color);
    }

    public void setWeekInfoBackgroundColor(@ColorInt int color) {
        mCalendarViewDelegate.setWeekInfoBackgroundColor(color);
        mWeekInfoView.postInvalidate();
    }

    public void setWeekInfoTextSize(int weekInfoTextSize) {
        mCalendarViewDelegate.setWeekInfoTextSize(weekInfoTextSize);
        mWeekInfoView.postInvalidate();
    }

    public void setWeekInfoTextColor(@ColorInt int color) {
        mCalendarViewDelegate.setWeekInfoTextColor(color);
        mWeekInfoView.postInvalidate();
    }

    public void setOnDateSelectedListener(OnDateSelectedListener onDateSelectedListener) {
        mOnDateSelectedListener = onDateSelectedListener;
    }

    public void setOnPageSelectedListener(OnPageSelectedListener onPageSelectedListener) {
        mOnPageSelectedListener = onPageSelectedListener;
    }

    public void setCurrentMonthOnClickListeners(View.OnClickListener listener) {
        currentMonthLabel.setOnClickListener(listener);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        measureChildren(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = currentMonthLabel.getMeasuredHeight();
        height += mWeekInfoView.getMeasuredHeight();
        if (mMonthPager.getVisibility() != GONE) {
            height += mMonthPager.getMeasuredHeight();
        } else {
            height += mWeekPager.getMeasuredHeight();
        }
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int monthLabelHeight = currentMonthLabel.getMeasuredHeight();
        currentMonthLabel.layout(0, 0, r, monthLabelHeight);
        int weekInfoHeight = mWeekInfoView.getMeasuredHeight();
        mWeekInfoView.layout(0, monthLabelHeight, r, monthLabelHeight + weekInfoHeight);
        if (mMonthPager.getVisibility() != GONE) {
            mMonthPager.layout(0, monthLabelHeight + weekInfoHeight, r, monthLabelHeight + weekInfoHeight + mMonthPager.getMeasuredHeight());
        }
        if (mWeekPager.getVisibility() != GONE) {
            mWeekPager.layout(0, monthLabelHeight + weekInfoHeight, r, monthLabelHeight + weekInfoHeight + mWeekPager.getMeasuredHeight());
        }
    }

    private void updateCalendar() {
        CalendarItemView calendarItemView = getCurrentCalendarItemView();
        if (calendarItemView != null) {
            calendarItemView.postInvalidate();
        }
        currentMonthLabel.postInvalidate();
    }

    private void setDateToCurrent() {
        final LocalDate localDate = new LocalDate();
        mCalendarViewDelegate.setSelectedDate(new Date(localDate.getYear(),
                localDate.getMonthOfYear(), localDate.getDayOfMonth()));
        post(new Runnable() {
            @Override
            public void run() {
                scrollToDate(localDate.getYear(), localDate.getMonthOfYear(),
                        localDate.getDayOfMonth(), false);
            }
        });

    }

    private void initChild() {
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        currentMonthLabel = new CurrentMonthView(getContext(), mCalendarViewDelegate);
        addView(currentMonthLabel, -1, layoutParams);

        mWeekInfoView = new WeekInfoView(getContext(), mCalendarViewDelegate);
        addView(mWeekInfoView, 0, layoutParams);

        MonthCalendarAdapter monthAdapter = new MonthCalendarAdapter(20000, mCalendarViewDelegate.getStartYear(),
                mCalendarViewDelegate.getStartMonth(), mCalendarViewDelegate);
        mMonthPager = new CalendarItemPager(getContext());
        mMonthPager.setOffscreenPageLimit(1);
        mMonthPager.setAdapter(monthAdapter);
        addView(mMonthPager, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        mWeekPager = new CalendarItemPager(getContext());
        WeekCalendarAdapter weekAdapter = new WeekCalendarAdapter(20000, mCalendarViewDelegate.getStartYear(),
                mCalendarViewDelegate.getStartMonth(), mCalendarViewDelegate);
        mWeekPager.setOffscreenPageLimit(1);
        mWeekPager.setAdapter(weekAdapter);
        addView(mWeekPager, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        mMonthPager.addOnPageChangeListener(mOnMonthPageChangeListener);
        mWeekPager.addOnPageChangeListener(mOnWeekPageChangeListener);
    }

    @Nullable
    private CalendarItemView getWeekCurrentItem() {
        return mWeekPager.findViewWithTag(mWeekPager.getCurrentItem());
    }
    @Nullable
    private CalendarItemView getMonthCurrentItem() {
        return mMonthPager.findViewWithTag(mMonthPager.getCurrentItem());
    }

    private void selectedMonthPage() {
        if (mOnPageSelectedListener == null) {
            return;
        }
        CalendarItemView calendarItemView = getMonthCurrentItem();
        if (calendarItemView == null) {
            return;
        }
        //Here, 15 can be any value in the middle, the purpose is to ensure that the date of
        //the current month is obtained, not the previous month or the next month.
        Date date = calendarItemView.getDates().get(15);
        List<Date> scheme = mOnPageSelectedListener.onPageSelected(new PagerInfo(
                date.getYear(), date.getMonth()));
        calendarItemView.setScheme(scheme);
    }

    private void selectedWeekPage() {
        if (mOnPageSelectedListener == null) {
            return;
        }
        CalendarItemView calendarItemView = getWeekCurrentItem();
        if (calendarItemView == null) {
            return;
        }
        Date mondayDate = calendarItemView.getDates().get(0);
        List<Date> scheme = mOnPageSelectedListener.onPageSelected(new PagerInfo(mondayDate.getYear(),
                mondayDate.getMonth(), mondayDate.getDay()));
        calendarItemView.setScheme(scheme);
    }

    private ViewPager.OnPageChangeListener mOnMonthPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
//            if (getCalendarType() != TYPE_MONTH) {
//                return;
//            }
            CalendarItemView calendarItemView = getMonthCurrentItem();
            if (calendarItemView == null) {
                return;
            }
            Date lastSelectedDate = mCalendarViewDelegate.getSelectedDate();
            //Here, 15 can be any value in the middle, the purpose is to ensure that the date of
            //the current month is obtained, not the previous month or the next month.
            Date date = calendarItemView.getDates().get(15);
            int day = lastSelectedDate.getDay();
            if (lastSelectedDate.getDay() > 28) {
                int maxDayOfMonth = CalendarUtil.getMaxDayByYearMonth(date.getYear(), date.getMonth());
                day = Math.min(maxDayOfMonth, lastSelectedDate.getDay());
            }
            calendarItemView.selectDate(new Date(date.getYear(), date.getMonth(), day));

            if (mOnPageSelectedListener != null && mCalendarType == CalendarViewType.MONTH) {
                selectedMonthPage();
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };

    private ViewPager.OnPageChangeListener mOnWeekPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
//            if (getCalendarType() != TYPE_WEEK) {
//                return;
//            }
            CalendarItemView calendarItemView = getWeekCurrentItem();

            if (calendarItemView == null) {
                return;
            }
            Date lastSelectedDate = mCalendarViewDelegate.getSelectedDate();
            //Week range is from 1 to 7, but it is translated in a range from 0 to 6
            calendarItemView.selectDate(lastSelectedDate.getWeek() - 1);
            if (mOnPageSelectedListener != null && mCalendarType == CalendarViewType.WEEK) {
                selectedWeekPage();
            }

        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };

}
