package progettomobile.balancemanager.calendarview.views;

import android.content.Context;
import android.graphics.Canvas;
import android.text.TextPaint;
import android.view.View;

import progettomobile.balancemanager.calendarview.CalendarViewDelegate;
import progettomobile.balancemanager.calendarview.utils.DensityUtil;

public class CurrentMonthView extends View {

    private Context context;
    private TextPaint textPaint;
    private CalendarViewDelegate calendarDelegate;

    public CurrentMonthView(final Context context, CalendarViewDelegate delegate) {
        super(context);
        this.calendarDelegate = delegate;
        super.setBackgroundColor(calendarDelegate.getWeekInfoBackgroundColor());
        initPaint();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int width = widthSize;
        int height = heightSize;
        if (widthMode == MeasureSpec.AT_MOST) {
            width = MeasureSpec.makeMeasureSpec((1 << 30) -1, MeasureSpec.AT_MOST);
        }
        if (heightMode == MeasureSpec.AT_MOST) {
            height = DensityUtil.dp2px(getContext(), 40);
        }
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        textPaint.setTextSize(calendarDelegate.getWeekInfoTextSize());
        textPaint.setColor(calendarDelegate.getWeekInfoTextColor());
        int itemWidth = getWidth();
        int itemHeight = getHeight();
        String text = calendarDelegate.getSelectedMonth() + " " + calendarDelegate.getSelectedYear();
        int startX = (int)((itemWidth - textPaint.measureText(text)) / 2);
        int startY = (int) (itemHeight / 2 - (textPaint.descent() + textPaint.ascent()) / 2);
        canvas.drawText(text, startX, startY, textPaint);
    }

    private void initPaint() {
        textPaint = new TextPaint();
        textPaint.setTextSize(calendarDelegate.getWeekInfoTextSize());
        textPaint.setColor(calendarDelegate.getWeekInfoTextColor());
        textPaint.setAntiAlias(true);
    }
}
