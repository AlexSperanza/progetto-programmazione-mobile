package progettomobile.balancemanager.calendarview.layout;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.AbsListView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import progettomobile.balancemanager.calendarview.basics.CalendarViewType;
import progettomobile.balancemanager.calendarview.exceptions.ViewNotFoundException;
import progettomobile.balancemanager.calendarview.pagers.CalendarItemPager;
import progettomobile.balancemanager.calendarview.views.CalendarItemView;
import progettomobile.balancemanager.calendarview.views.CalendarView;

public class CalendarLayout extends LinearLayout {

    private CalendarView mCalendarView;
    private View mContentView;
    private float mMoveY, mLastDownY;
    private int mTouchSlop;

    private CalendarItemPager mMonthPager;

    private int mContentViewMaxTranslationY;

    private int mMonthViewSelectedItemTop;

    private int mContentViewOverScrollMode;

    private boolean mIsAnimating;

    private boolean mEnableScroll = true;

    public CalendarLayout(Context context) {
        this(context, null);
    }

    public CalendarLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CalendarLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        final ViewConfiguration configuration = ViewConfiguration.get(context);

        mTouchSlop = configuration.getScaledTouchSlop();
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (!mEnableScroll) {
            return super.onInterceptTouchEvent(ev);
        }
        updateSelectedItemTop();
        if (mIsAnimating) {
            return true;
        }
        float y = ev.getY();
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mMoveY = mLastDownY = y;
                break;
            case MotionEvent.ACTION_MOVE:

                float dy = y - mMoveY;
                if (Math.abs(dy) < mTouchSlop) {
                    return false;
                }
                if (mCalendarView.getCalendarType() == CalendarViewType.MONTH && dy < 0) {
                    mContentView.setOverScrollMode(View.OVER_SCROLL_NEVER);
                    return true;
                } else if (mCalendarView.getCalendarType() == CalendarViewType.WEEK && dy > 0) {
                    mContentView.setOverScrollMode(View.OVER_SCROLL_NEVER);
                    return true;
                }
                break;
            case MotionEvent.ACTION_UP:
                break;
        }
        return super.onInterceptTouchEvent(ev);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!mEnableScroll) {
            return super.onTouchEvent(event);
        }
        if (mIsAnimating) {
            return true;
        }
        float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mMoveY = y;
                break;
            case MotionEvent.ACTION_MOVE:
                float dy = y - mMoveY;
                mMoveY = y;
                float contentViewTransY = mContentView.getTranslationY();
                if (!isScrollToTop() || (dy < 0 && mMonthPager.getVisibility() != VISIBLE)) {
                    //Propagates sliding
                    return mContentView.onTouchEvent(event);
                }
                //Show week mode on swipe up
                if (dy < 0 && contentViewTransY == -mContentViewMaxTranslationY) {
                    showWeek();
                    return true;
                }
                if (dy > 0 && contentViewTransY == 0) {
                    if (mMonthPager.getVisibility() == VISIBLE) {
                        mCalendarView.setTypeToMonth();
                    } else {
                        hideWeek();
                    }
                }
                scrollChild(dy);
                return true;
            case MotionEvent.ACTION_UP:
                if (mContentView.getTranslationY() == 0) { //If 0, no motion done
                    return true;
                }
                float moveY = event.getY() - mLastDownY;
                CalendarViewType type = mCalendarView.getCalendarType();
                if (type == CalendarViewType.MONTH) {
                    if (moveY < -100) {
                        shrink();
                    } else {
                        expand();
                    }
                } else {
                    if (moveY > 100) {
                        expand();
                    } else {
                        shrink();
                    }
                }
                return true;
        }
        return super.onTouchEvent(event);
    }

    public void expand() {
        mContentView.setOverScrollMode(mContentViewOverScrollMode);
        if (mIsAnimating) {
            return;
        }
        mIsAnimating = true;
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(mContentView,
                "translationY", mContentView.getTranslationY(), 0);
        objectAnimator.setDuration(500);
        objectAnimator.addUpdateListener(animation -> {
            float currentValue = (Float) animation.getAnimatedValue();
            if (mMonthPager.getTranslationY() <= mContentView.getTranslationY()) {
                mMonthPager.setTranslationY(currentValue);
            }
        });
        objectAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mIsAnimating = false;
                mCalendarView.setTypeToMonth();
            }
        });
        objectAnimator.start();
    }

    public void shrink() {
        mContentView.setOverScrollMode(mContentViewOverScrollMode);
        if (mIsAnimating) {
            return;
        }

        mIsAnimating = true;
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(mContentView,
                "translationY", mContentView.getTranslationY(), -mContentViewMaxTranslationY);
        objectAnimator.setDuration(500);
        objectAnimator.addUpdateListener(animation -> {
            float currentValue = (Float) animation.getAnimatedValue();
            if (currentValue > -mMonthViewSelectedItemTop) {
                mMonthPager.setTranslationY(currentValue);
            } else {
                mMonthPager.setTranslationY(-mMonthViewSelectedItemTop);
            }
        });
        objectAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mIsAnimating = false;
                showWeek();
            }
        });
        objectAnimator.start();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (getChildCount() == 2 && getChildAt(0) instanceof CalendarView) {
            mCalendarView = (CalendarView) getChildAt(0);

            mMonthPager = mCalendarView.getMonthPager();
        } else {
            throw new ViewNotFoundException(getClass().getName() + " must be 2 child view, " +
                    "and first child is progettomobile.balancemanager.calendarview.views.CalendarView");
        }
        mContentView = getChildAt(1);
        mContentViewOverScrollMode = mContentView.getOverScrollMode();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (!mEnableScroll) {
            return;
        }
        //Expand content view to cover gaps
        if (mCalendarView.getMonthPager().getVisibility() == VISIBLE) {
            int monthPagerHeight = mCalendarView.getMonthPager().getMeasuredHeight();
            mContentViewMaxTranslationY = monthPagerHeight - monthPagerHeight / CalendarItemView.MAX_ROW;
            int contentViewHeight = mContentView.getMeasuredHeight() + mContentViewMaxTranslationY;
            int heightSpec = MeasureSpec.makeMeasureSpec(contentViewHeight, MeasureSpec.EXACTLY);
            mContentView.measure(widthMeasureSpec, heightSpec);
        }
        //If the translation is 0, calculate its value
        if (mContentViewMaxTranslationY == 0 && mCalendarView.getMonthPager().getVisibility() == GONE) {
            int weekPagerHeight = mCalendarView.getWeekPager().getMeasuredHeight();
            mContentViewMaxTranslationY = weekPagerHeight * (CalendarItemView.MAX_ROW - 1);
        }
    }

    private void updateSelectedItemTop() {
        CalendarItemView calendarItemView =  mMonthPager.findViewWithTag(mMonthPager.getCurrentItem());
        if (calendarItemView == null) {
            return;
        }
        mMonthViewSelectedItemTop = calendarItemView.getSelectedItemTop();
    }

    private boolean isScrollToTop() {
        if (mContentView instanceof RecyclerView)
            return ((RecyclerView) mContentView).computeVerticalScrollOffset() == 0;
        if (mContentView instanceof AbsListView) {
            boolean result = false;
            AbsListView listView = (AbsListView) mContentView;
            if (listView.getFirstVisiblePosition() == 0) {
                final View topChildView = listView.getChildAt(0);
                result = topChildView.getTop() == 0;
            }
            return result;
        }
        return mContentView.getScrollY() == 0;
    }

    private void scrollChild(float dy) {
        if (dy > 0) { // Swipe down event
            if (mContentView.getTranslationY() + dy > 0) {
                mContentView.setTranslationY(0);
            } else {
                mContentView.setTranslationY(mContentView.getTranslationY() + dy);
            }
            //Slide MonthPager after ContentView sliding
            if (mMonthPager.getTranslationY() <= mContentView.getTranslationY()) {
                mMonthPager.setTranslationY(mContentView.getTranslationY());
            }
        } else if (dy < 0) {    //Swipe up event
            if (-mContentView.getTranslationY() < mContentViewMaxTranslationY) {
                mContentView.setTranslationY(Math.max(mContentView.getTranslationY() + dy, -mContentViewMaxTranslationY));
            }
            if (mMonthPager.getTranslationY() + dy > -mMonthViewSelectedItemTop) {
                mMonthPager.setTranslationY(mContentView.getTranslationY());
            } else {
                mMonthPager.setTranslationY(-mMonthViewSelectedItemTop);
            }
        }
    }

    private void hideWeek() {
        mCalendarView.getMonthPager().setVisibility(VISIBLE);
        mCalendarView.scrollToSelectedDate();
        updateSelectedItemTop();
        CalendarItemView calendarItemView = mMonthPager.findViewWithTag(mMonthPager.getCurrentItem());
        if (calendarItemView != null) {
            mMonthPager.setTranslationY(-calendarItemView.getSelectedItemTop());
            mContentView.setTranslationY(-mContentViewMaxTranslationY);
        }
        mCalendarView.getWeekPager().setVisibility(GONE);
    }

    private void showWeek() {
        mContentView.setTranslationY(0);
        mCalendarView.getMonthPager().setVisibility(GONE);
        mCalendarView.getWeekPager().setVisibility(VISIBLE);
        mCalendarView.setTypeToWeek();
        mCalendarView.scrollToSelectedDate();
    }
}
