package progettomobile.balancemanager.calendarview.pagers;

import progettomobile.balancemanager.calendarview.basics.CalendarViewType;

public class PagerInfo {

    private CalendarViewType type;
    private int year;
    private int month;
    private int mondayDay;

    public PagerInfo(int year, int month) {
        setDate(year, month);
    }

    public PagerInfo(int year, int month, int mondayDay) {
        setDate(year, month, mondayDay);
    }


    public CalendarViewType getType() {
        return type;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getMondayDay() {
        return mondayDay;
    }

    private void setDate(int year, int month, int mondayDay) {
        this.year = year;
        this.month = month;
        this.mondayDay = mondayDay;
        type = CalendarViewType.WEEK;
    }

    private void setDate(int year, int month) {
        type = CalendarViewType.MONTH;
        this.year = year;
        this.month = month;
    }
}
