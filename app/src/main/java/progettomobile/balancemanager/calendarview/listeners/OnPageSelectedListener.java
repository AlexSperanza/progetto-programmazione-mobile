package progettomobile.balancemanager.calendarview.listeners;

import androidx.annotation.NonNull;

import progettomobile.balancemanager.calendarview.basics.Date;
import progettomobile.balancemanager.calendarview.pagers.PagerInfo;

import java.util.List;

/**
 * The listener is called when the page switches
 */
public interface OnPageSelectedListener {
    /**
     * Called after the month switch
     * @param pagerInfo current page information
     * @return returns Scheme of the current month, or null if there is no
     */
    List<Date> onPageSelected(@NonNull PagerInfo pagerInfo);


}