package progettomobile.balancemanager.calendarview.listeners;

import progettomobile.balancemanager.calendarview.basics.Date;

public interface OnDateSelectedListener {
    void onDateSelected(Date date);
}
