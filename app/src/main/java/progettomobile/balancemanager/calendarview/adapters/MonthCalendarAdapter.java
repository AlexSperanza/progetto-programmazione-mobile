package progettomobile.balancemanager.calendarview.adapters;


import progettomobile.balancemanager.calendarview.CalendarViewDelegate;
import progettomobile.balancemanager.calendarview.basics.Date;
import progettomobile.balancemanager.calendarview.utils.CalendarUtil;

import java.util.List;

public class MonthCalendarAdapter extends BaseCalendarAdapter {

    public MonthCalendarAdapter(int count, int startYear, int startMonth, CalendarViewDelegate calendarViewDelegate) {
        super(count, startYear, startMonth, calendarViewDelegate);
    }

    @Override
    public List<Date> getDateList(int startYear, int startMonth, int position) {
        int[] date = CalendarUtil.positionToDate(position, startYear, startMonth);

        return CalendarUtil.getMonthDates(date[0], date[1]);
    }

}
