package progettomobile.balancemanager.calendarview.adapters;

import progettomobile.balancemanager.calendarview.CalendarViewDelegate;
import progettomobile.balancemanager.calendarview.basics.Date;
import progettomobile.balancemanager.calendarview.utils.CalendarUtil;

import java.util.List;

public class WeekCalendarAdapter extends BaseCalendarAdapter {
    public WeekCalendarAdapter(int count, int startYear, int startMonth, CalendarViewDelegate calendarViewDelegate) {
        super(count, startYear, startMonth, calendarViewDelegate);
    }

    @Override
    public List<Date> getDateList(int startYear, int startMonth, int position) {
        return CalendarUtil.getWeekDaysForPosition(startYear, startMonth, 1, position);
    }

}
