package progettomobile.balancemanager.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import progettomobile.balancemanager.database.daos.AccountDao;
import progettomobile.balancemanager.database.daos.CategoryDao;
import progettomobile.balancemanager.database.daos.TransactionDao;
import progettomobile.balancemanager.database.entities.Account;
import progettomobile.balancemanager.database.entities.Category;
import progettomobile.balancemanager.database.entities.Transaction;

@Database(entities = {Account.class, Transaction.class, Category.class}, version = 1, exportSchema = false)
@TypeConverters(Transaction.DateConverters.class)
public abstract class AppDatabase extends RoomDatabase {
    public abstract AccountDao accountDao();
    public abstract TransactionDao transactionDao();
    public abstract CategoryDao categoryDao();
}
