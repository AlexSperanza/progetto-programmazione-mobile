package progettomobile.balancemanager.database.daos;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import progettomobile.balancemanager.database.entities.Category;

import java.util.List;

@Dao
public interface CategoryDao {
    @Insert
    long insertCategory(Category category);

    @Update
    void updateCategory(Category category);

    @Delete
    void deleteCategory(Category category);

    @Query("SELECT * FROM CATEGORY")
    List<Category> getCategoryList();

    @Query("SELECT ifnull(MAX(idCategory), 0) FROM Category")
    long getLastId();

}
