package progettomobile.balancemanager.database.daos;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import progettomobile.balancemanager.database.entities.Account;


import java.util.List;

@Dao
public interface AccountDao {
    @Insert
    long insertAccount(Account account);

    @Update
    void updateAccount(Account account);

    @Delete
    void deleteAccount(Account account);

    @Query("SELECT * FROM Account")
    List<Account> getAccountList();

    /**
     * Returns the last used id.
     * If 0, no entity is in the table.
     * In any case, a new id must be at least LastId + 1.
     */
    @Query("SELECT ifnull( MAX(idAccount), 0) FROM Account")
    long getLastId();

    @Query("SELECT * FROM Account WHERE idAccount = :accountId")
    Account getAccount(long accountId);

    @Query("UPDATE account SET account_balance = (SELECT sum(cent_value) FROM `transaction` WHERE `transaction`.accountId = account.idAccount)")
    void recalculateAccountBalances();

}
