package progettomobile.balancemanager.database.daos;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import progettomobile.balancemanager.database.entities.Transaction;
import progettomobile.balancemanager.database.relations.TransactionWithCategory;

import java.util.List;

@Dao
public interface TransactionDao {
    @Insert
    long insertTransaction(Transaction transaction);

    @Update
    void updateTransaction(Transaction transaction);

    @Delete
    void deleteTransaction(Transaction transaction);

    @Query("SELECT ifnull(MAX(idTransaction), 0) FROM `Transaction`")
    long getLastId();

    @Query("SELECT * FROM `transaction` join category on idCategory = category_id WHERE accountId = :accountId ORDER BY date DESC")
    List<TransactionWithCategory> getTransactionsOf(long accountId);

    @Query("UPDATE `transaction` SET category_id = 4 WHERE category_id = :categoryId")
    void validateTransactionOnCategoryDeletion(long categoryId);

    @Query("DELETE FROM `transaction` WHERE accountId = :accountId OR target_account_id = :accountId")
    void deleteTransactionsOf(long accountId);

    @Query("DELETE FROM `transaction` WHERE idTransaction = :targetTransactionId")
    void deleteTransfer(long targetTransactionId);
}
