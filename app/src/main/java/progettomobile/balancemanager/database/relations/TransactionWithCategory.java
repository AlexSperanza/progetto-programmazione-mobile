package progettomobile.balancemanager.database.relations;

import androidx.room.Embedded;

import progettomobile.balancemanager.database.entities.Category;
import progettomobile.balancemanager.database.entities.Transaction;

public class TransactionWithCategory {
    @Embedded
    private Transaction transaction;
    @Embedded
    private Category category;

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
