package progettomobile.balancemanager.database.relations;

import androidx.room.Embedded;
import androidx.room.Relation;

import progettomobile.balancemanager.database.entities.Account;
import progettomobile.balancemanager.database.entities.Transaction;

import java.util.List;

public class AccountWithCategorizedTransactions {
    @Embedded private Account account;
    @Relation(
        entity = Transaction.class,
        parentColumn = "idAccount",
        entityColumn = "accountId"
    )
    private List<TransactionWithCategory> transactions;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public List<TransactionWithCategory> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<TransactionWithCategory> transactions) {
        this.transactions = transactions;
    }
}
