package progettomobile.balancemanager.database;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import progettomobile.balancemanager.activities.DataSyncListener;
import progettomobile.balancemanager.appwidget.providers.SingleAccountAppWidgetProvider;
import progettomobile.balancemanager.constants.SharedPreferencesKeys;
import progettomobile.balancemanager.database.entities.Account;
import progettomobile.balancemanager.database.entities.AccountBuilder;
import progettomobile.balancemanager.database.entities.Category;
import progettomobile.balancemanager.database.entities.CategoryBuilder;
import progettomobile.balancemanager.database.entities.Transaction;
import progettomobile.balancemanager.database.relations.TransactionWithCategory;

/**
 * Singleton which holds the database instance
 */
public class Database {
    private static List<Pair<DataSyncListener, Runnable>> dataObservers = new ArrayList<>();

    private static long currentAccountId;
    private static List<Account> accountList = new ArrayList<>();
    private static List<Category> categoryList = new ArrayList<>();
    private static long lastTransactionId;
    private static Map<LocalDate, List<TransactionWithCategory>> currentAccountTransactions = new HashMap<>();

    private static AppDatabase dbInstance;
    private static ExecutorService databaseExecService;
    private static Context applicationContext;

    private Database() {
    }

    public static void initAppDatabase(@NonNull Context context) {
        Database.applicationContext = context;
        if (dbInstance == null) {
            dbInstance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "app-database")
                    .addCallback(new RoomDatabase.Callback() {
                        @Override
                        public void onCreate(@NonNull SupportSQLiteDatabase db) {
                            super.onCreate(db);
                            Database.addDefaultCategories();
                        }
                    })
                    .build();
        }
        if (databaseExecService == null) {
            databaseExecService = Executors.newFixedThreadPool(4);
        }
        syncData();
    }

    public static AppDatabase getDbInstance() {
        return dbInstance;
    }

    public static void syncData() {
        if (currentAccountId == SharedPreferencesKeys.NO_CURRENT_ACCOUNT ||
                currentAccountId != Database.applicationContext
                        .getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE)
                        .getLong(SharedPreferencesKeys.LAST_ACCOUNT, SharedPreferencesKeys.NO_CURRENT_ACCOUNT)) {
            try {
                databaseExecService.execute(() -> dbInstance.accountDao().recalculateAccountBalances());
                currentAccountId = Database.applicationContext
                        .getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE)
                        .getLong(SharedPreferencesKeys.LAST_ACCOUNT, SharedPreferencesKeys.NO_CURRENT_ACCOUNT);
                Future<List<Account>> futureAccountList = databaseExecService.submit(() -> dbInstance.accountDao().getAccountList());
                Future<List<Category>> futureCategoryList = databaseExecService.submit(() -> dbInstance.categoryDao().getCategoryList());
                Future<List<TransactionWithCategory>> futureTransactionList = databaseExecService.submit(() -> dbInstance.transactionDao().getTransactionsOf(currentAccountId));
                Future<Long> futureLastTransactionId = databaseExecService.submit(() -> dbInstance.transactionDao().getLastId());
                accountList.clear();
                categoryList.clear();
                currentAccountTransactions.clear();
                accountList = futureAccountList.get();
                categoryList = futureCategoryList.get();
                lastTransactionId = futureLastTransactionId.get();
                List<TransactionWithCategory> transactionWithCategoryList = futureTransactionList.get();
                for (TransactionWithCategory twc : transactionWithCategoryList) {
                    if (!currentAccountTransactions.containsKey(twc.getTransaction().getDate())) {
                        currentAccountTransactions.put(twc.getTransaction().getDate(), new ArrayList<>());
                    }
                    Objects.requireNonNull(currentAccountTransactions.get(twc.getTransaction().getDate())).add(twc);
                }
                AccountBuilder.setLastAccountId(getLastAccountId());
                CategoryBuilder.setLastCategoryId(getLastCategoryId());

            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        for (int i = 0; i < dataObservers.size(); i++) {
            if (dataObservers.get(i).first != null) {
                Objects.requireNonNull(dataObservers.get(i).first).onDataSynced(dataObservers.get(i).second);
            }
        }
    }

    public static void registerObserver(@NonNull DataSyncListener newListener, Runnable callback) {
        Pair<DataSyncListener, Runnable> newPair = new Pair<>(newListener, callback);
        if (!dataObservers.contains(newPair)) {
            dataObservers.add(newPair);
        }
    }

    public static void removeObserver(DataSyncListener listener) {
        for (int i = 0; i < dataObservers.size(); i++) {
            if (dataObservers.get(i).first == listener) {
                dataObservers.remove(i--);
            }
        }
    }

    /*
     * Account methods
     */
    @Nullable
    public static Account getCurrentAccount() {
        return findAccount(currentAccountId);
    }

    public static long getLastAccountId() {
        long max = 0;
        if (accountList != null) {
            for (int i = 0; i < accountList.size(); i++) {
                if (max < accountList.get(i).getIdAccount()) {
                    max = accountList.get(i).getIdAccount();
                }
            }
        }
        return max;
    }

    public static void insertAccount(@NonNull Account account) {
        accountList.add(account);
        databaseExecService.execute(() -> dbInstance.accountDao().insertAccount(account));
    }

    public static void updateAccount(@NonNull Account account) {
        replaceAccount(account);
        databaseExecService.execute(() -> dbInstance.accountDao().updateAccount(account));
        notifyAppWidgets();
    }

    public static void deleteAccount(@NonNull Account account) {
        if (account.getIdAccount() == currentAccountId) { // Update current account id
            applicationContext.getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE)
                    .edit()
                    .putLong(SharedPreferencesKeys.LAST_ACCOUNT, account.getIdAccount() == accountList.get(0).getIdAccount() ? accountList.get(1).getIdAccount() : accountList.get(0).getIdAccount())
                    .apply();
            currentAccountTransactions.clear();
        }
        accountList.remove(account);
        //Forces the sync of data
        currentAccountId = SharedPreferencesKeys.NO_CURRENT_ACCOUNT;

        applicationContext.getSharedPreferences(SharedPreferencesKeys.ACCOUNT_PREFIX+account.getIdAccount(), Context.MODE_PRIVATE)
                .edit()
                .clear()
                .apply();
        databaseExecService.execute(() -> {
            dbInstance.transactionDao().deleteTransactionsOf(account.getIdAccount());
            dbInstance.accountDao().deleteAccount(account);
        });
        notifyAppWidgets();
    }

    public static List<Account> getAccountList() {
        return accountList;
    }

    public static Account findAccount(long accountId) {
        for (Account account : accountList) {
            if (account.getIdAccount() == accountId) {
                return account;
            }
        }
        return null;
    }

    public static int indexOfAccount(long accountId) {
        for (int i = 0; i < accountList.size(); i++) {
            if (accountList.get(i).getIdAccount() == accountId) {
                return i;
            }
        }
        return -1;
    }

    private static void replaceAccount(@NonNull Account account) {
        int index = indexOfAccount(account.getIdAccount());
        if (index >= 0) {
            accountList.set(index, account);
        }
    }

    private static void notifyAppWidgets() {
        Intent intent = new Intent(applicationContext, SingleAccountAppWidgetProvider.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        int[] ids = AppWidgetManager.getInstance(applicationContext).getAppWidgetIds(new ComponentName(applicationContext, SingleAccountAppWidgetProvider.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        applicationContext.sendBroadcast(intent);
    }

    /*
     * Category methods
     */
    public static List<Category> getCategoryList() {
        return categoryList;
    }

    public static Category findCategory(long categoryId) {
        for (Category category : categoryList) {
            if (category.getIdCategory() == categoryId) {
                return category;
            }
        }
        return null;
    }

    public static long getLastCategoryId() {
        long max = 0;
        for (int i = 0; i < categoryList.size(); i++) {
            if (max < categoryList.get(i).getIdCategory()) {
                max = categoryList.get(i).getIdCategory();
            }
        }
        return max;
    }

    public static void insertCategory(@NonNull Category category) {
        categoryList.add(category);
        databaseExecService.execute(() -> dbInstance.categoryDao().insertCategory(category));
    }

    public static void updateCategory(@NonNull Category category) {
        replaceCategory(category);
        databaseExecService.execute(() -> dbInstance.categoryDao().updateCategory(category));
    }

    public static void deleteCategory(@NonNull Category category) {
        validateTransactionsOnCategoryDelete(category.getIdCategory());
        categoryList.remove(category);
        databaseExecService.execute(() -> dbInstance.categoryDao().deleteCategory(category));
    }

    public static int indexOfCategory(long categoryId) {
        for (int i = 0; i < categoryList.size(); i++) {
            if (categoryList.get(i).getIdCategory() == categoryId) {
                return i;
            }
        }
        return -1;
    }

    private static void replaceCategory(@NonNull Category category) {
        int index = indexOfCategory(category.getIdCategory());
        if (index >= 0) {
            categoryList.set(index, category);
        }
    }

    private static void addDefaultCategories() {
        insertCategory(new CategoryBuilder()
                .setId(1)
                .setCategoryName("ACCOUNT_CREATION")
                .setIsDefault(true)
                .setIsUnchoosable(true)
                .build());
        insertCategory(new CategoryBuilder()
                .setId(2)
                .setCategoryName("BALANCE_CORRECTION")
                .setIsDefault(true)
                .setIsUnchoosable(true)
                .build());
        insertCategory(new CategoryBuilder().setId(3)
                .setCategoryName("TRANSFER")
                .setIsDefault(true)
                .setIsUnchoosable(true)
                .build());
        insertCategory(new CategoryBuilder().setId(4)
                .setCategoryName("FOOD")
                .setIsDefault(true)
                .setIsUnchoosable(false)
                .build());
        insertCategory(new CategoryBuilder().setId(5)
                .setCategoryName("CLOTHES")
                .setIsDefault(true)
                .setIsUnchoosable(false)
                .build());
    }

    /*
     * Transaction methods
     */
    public static Map<LocalDate, List<TransactionWithCategory>> getCurrentAccountTransactions() {
        return currentAccountTransactions;
    }

    public static void insertTransaction(@NonNull Transaction transaction) {
        if (transaction.getAccountId() == currentAccountId) {
            if (!currentAccountTransactions.containsKey(transaction.getDate())) {
                currentAccountTransactions.put(transaction.getDate(), new ArrayList<>());
            }
            TransactionWithCategory transactionWithCategory = new TransactionWithCategory();
            transactionWithCategory.setTransaction(transaction);
            for (int i = 0; i < categoryList.size() && transactionWithCategory.getCategory() == null; i++) {
                if (categoryList.get(i).getIdCategory() == transaction.getCategoryId()) {
                    transactionWithCategory.setCategory(categoryList.get(i));
                }
            }
            Objects.requireNonNull(currentAccountTransactions.get(transaction.getDate())).add(transactionWithCategory);
        }
        if (transaction.getIdTransaction() == lastTransactionId + 1) {
            lastTransactionId = lastTransactionId + 1;
        }
        Account account = findAccount(transaction.getAccountId());
        if (account != null) {
            updateAccount(account);
        }
        databaseExecService.execute(() -> dbInstance.transactionDao().insertTransaction(transaction));
    }

    public static Transaction findTransaction(long transactionId, LocalDate date) {
        List<TransactionWithCategory> dateTransactionList;
        if (currentAccountTransactions.containsKey(date)) {
            dateTransactionList = currentAccountTransactions.get(date);
        } else {
            dateTransactionList = new ArrayList<>();
        }
        if (dateTransactionList != null) {
            for (TransactionWithCategory twc :
                    dateTransactionList) {
                if (twc.getTransaction().getIdTransaction() == transactionId) {
                    return twc.getTransaction();
                }
            }
        }
        return null;
    }

    public static void updateTransaction(@NonNull Transaction transaction) {
        Account account = findAccount(transaction.getAccountId());
        if (account != null) {
            updateAccount(account);
        }
        TransactionWithCategory transactionWithCategory = new TransactionWithCategory();
        transactionWithCategory.setTransaction(transaction);
        for (int i = 0; i < categoryList.size() && transactionWithCategory.getCategory() == null; i++) {
            if (categoryList.get(i).getIdCategory() == transaction.getCategoryId()) {
                transactionWithCategory.setCategory(categoryList.get(i));
            }
        }
        if (currentAccountTransactions.containsKey(transaction.getDate())) {
            List<TransactionWithCategory> transactionList = currentAccountTransactions.get(transaction.getDate());
            for (int i = 0; i < transactionList.size(); i++) {
                if (transactionList.get(i).getTransaction().getIdTransaction() == transaction.getIdTransaction()) {
                    transactionList.remove(i);
                    transactionList.add(i, transactionWithCategory);
                }
            }
        }
        databaseExecService.execute(() -> dbInstance.transactionDao().updateTransaction(transaction));
    }

    public static void deleteTransaction(@NonNull Transaction transaction) {
        if (currentAccountTransactions.containsKey(transaction.getDate())) {
            List<TransactionWithCategory> transactionList = currentAccountTransactions.get(transaction.getDate());
            if (transactionList != null) {
                for (TransactionWithCategory twc : transactionList) {
                    if (twc.getTransaction().getIdTransaction() == transaction.getIdTransaction()) {
                        transactionList.remove(twc);
                        if (transaction.getIdTransaction() == lastTransactionId) {
                            lastTransactionId = lastTransactionId - 1;
                        }
                        Account account = findAccount(transaction.getAccountId());
                        if (account != null) {
                            updateAccount(account);
                        }
                        databaseExecService.execute(() -> dbInstance.transactionDao().deleteTransaction(transaction));
                        break;
                    }
                }
            }
        }
    }

    public static void deleteCounterTransfer(@NonNull Transaction transfer) {
        databaseExecService.execute(() -> dbInstance.transactionDao().deleteTransfer(transfer.getTargetTransactionId()));
        Account account = findAccount(transfer.getTargetAccountId());
        if (account != null) {
            updateAccount(account);
        }
    }

    public static long getLastTransactionId() {
        return lastTransactionId;
    }

    private static void validateTransactionsOnCategoryDelete(long categoryId) {
        if (categoryId < 6) {
            return;
        }
        List<TransactionWithCategory> currentTransactions = new ArrayList<>();
        for (LocalDate date : currentAccountTransactions.keySet()) {
            currentTransactions.addAll(currentAccountTransactions.get(date));
        }
        Category selectableCategory = findCategory(4); // Category: food
        for (TransactionWithCategory twc : currentTransactions) {
            twc.setCategory(selectableCategory);
        }
        databaseExecService.execute(() -> dbInstance.transactionDao().validateTransactionOnCategoryDeletion(categoryId));
    }
}
