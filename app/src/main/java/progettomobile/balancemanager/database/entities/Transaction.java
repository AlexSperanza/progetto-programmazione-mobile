package progettomobile.balancemanager.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;

import org.joda.time.LocalDate;
import org.joda.time.format.ISODateTimeFormat;

@Entity
public class Transaction {
    @PrimaryKey
    private long idTransaction;

    @ForeignKey(entity = Account.class, parentColumns = "accountId", childColumns = "accountId")
    private long accountId;

    @ColumnInfo(typeAffinity = 2)
    private LocalDate date;

    @ColumnInfo(name = "cent_value")
    private int centValue;

    @ColumnInfo(typeAffinity = 2)
    private String description;

    @ColumnInfo(name = "category_id")
    private long categoryId;

    /* If the targetAccountId is 0, then there is no target account.
     */
    @ColumnInfo(name = "target_account_id")
    private long targetAccountId;

    @ColumnInfo(name = "target_transaction_info")
    private long targetTransactionId;

    public Transaction() {
    }

    public Transaction(long transactionId, long accountId, LocalDate date, int value,
                       String description, long categoryId, long targetAccountId, long targetTransactionId) {
        this.idTransaction = transactionId;
        this.accountId = accountId;
        this.date = date;
        this.centValue = value;
        this.description = description;
        this.categoryId = categoryId;
        this.targetAccountId = targetAccountId;
        this.targetTransactionId = targetTransactionId;
    }

    public long getIdTransaction() {
        return idTransaction;
    }

    public void setIdTransaction(long idTransaction) {
        this.idTransaction = idTransaction;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getCentValue() {
        return centValue;
    }

    public void setCentValue(int centValue) {
        this.centValue = centValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public long getTargetAccountId() {
        return targetAccountId;
    }

    public void setTargetAccountId(long targetAccountId) {
        this.targetAccountId = targetAccountId;
    }

    public long getTargetTransactionId() {
        return targetTransactionId;
    }

    public void setTargetTransactionId(long targetTransactionId) {
        this.targetTransactionId = targetTransactionId;
    }

    public static class DateConverters {
        @TypeConverter
        public LocalDate fromDateString(String text) {
            return text == null ? null : LocalDate.parse(text, ISODateTimeFormat.date());
        }

        @TypeConverter
        public String dateToString(LocalDate date) {
            return date.toString();
        }
    }
}
