package progettomobile.balancemanager.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Account {
    @PrimaryKey
    private long idAccount;

    @ColumnInfo(name = "account_name")
    private String accountName;

    /*
     * The balance of the account is saved in cents to avoid precision problems.
     */
    @ColumnInfo(name = "account_balance")
    private int accountBalance;

    public Account() {

    }

    public Account(final long newId, final String name, final int balance) {
        this.idAccount = newId;
        this.accountName = name;
        this.accountBalance = balance;
    }

    public long getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(long pidAccount) {
        idAccount = pidAccount;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public int getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(int accountBalance) {
        this.accountBalance = accountBalance;
    }
}
