package progettomobile.balancemanager.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import progettomobile.balancemanager.R;

import java.util.HashMap;
import java.util.Map;

@Entity
public class Category {

    public static final Map<String, Integer> DEFAULT_STRING_MAP = createMap();

    @PrimaryKey
    private long idCategory;

    @ColumnInfo(name = "category_name")
    private String categoryName;

    /* A default category is not modifiable and
     * its string value is defined inside android resources.
     */
    @ColumnInfo(name = "is_default")
    private boolean isDefault;

    /* An unchoosable category cannot be chosen when creating a new transaction.
     */
    @ColumnInfo(name = "is_unchoosable")
    private boolean isUnchoosable;

    public Category() {

    }

    public Category(long newId, String name, boolean isDefault, boolean isUnchoosable) {
        this.idCategory = newId;
        this.categoryName = name;
        this.isDefault = isDefault;
        this.isUnchoosable = isUnchoosable;
    }

    public long getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(long idCategory) {
        this.idCategory = idCategory;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }

    public boolean isUnchoosable() {
        return isUnchoosable;
    }

    public void setUnchoosable(boolean unchoosable) {
        isUnchoosable = unchoosable;
    }

    private static Map<String, Integer> createMap() {
        Map<String, Integer> map = new HashMap<>();
        map.put("ACCOUNT_CREATION", R.string.account_creation_category_title);
        map.put("BALANCE_CORRECTION", R.string.balance_correction_category_title);
        map.put("TRANSFER", R.string.transfer_category_title);
        map.put("FOOD", R.string.food_category_title);
        map.put("CLOTHES", R.string.clothes_category_title);
        return map;
    }
}
