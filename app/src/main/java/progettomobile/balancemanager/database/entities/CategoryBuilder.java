package progettomobile.balancemanager.database.entities;

public class CategoryBuilder {
    private static long lastCategoryId;

    private long categoryId;
    private String categoryName;
    private boolean isDefault;
    private boolean isUnchoosable;
    private boolean isBuilt;

    public CategoryBuilder setId(long id) {
        categoryId = id;
        return this;
    }

    public CategoryBuilder useFirstFreeId() {
        categoryId = lastCategoryId + 1;
        return this;
    }

    public CategoryBuilder setCategoryName(String name) {
        this.categoryName = name;
        return this;
    }

    public CategoryBuilder setIsDefault(boolean value) {
        this.isDefault = value;
        return this;
    }

    public CategoryBuilder setIsUnchoosable(boolean value) {
        this.isUnchoosable = value;
        return this;
    }

    public CategoryBuilder setUserCategory() {
        return setIsDefault(false).setIsUnchoosable(false);
    }

    public Category build() {
        if (isBuilt) {
            throw new IllegalStateException("This builder has already built a Category. Use another instance to build another Category.");
        } else {
            isBuilt = true;
            if (categoryId == lastCategoryId + 1) {
                lastCategoryId++;
            }
            return new Category(categoryId, categoryName, isDefault, isUnchoosable);
        }
    }

    public static void setLastCategoryId(long lastCategoryId) {
        CategoryBuilder.lastCategoryId = lastCategoryId;
    }
}
