package progettomobile.balancemanager.database.entities;

import androidx.annotation.NonNull;

public class AccountBuilder {
    private static long lastAccountId;

    private long accountId;
    private String accountName = "";
    private int accountBalance = 0;
    private boolean isBuilt = false;

    public AccountBuilder setId(long id) {
        accountId = id;
        return this;
    }

    public AccountBuilder useFirstFreeId() {
        accountId = lastAccountId + 1;
        return this;
    }

    public AccountBuilder setAccountName(String name) {
        this.accountName = name;
        return this;
    }

    public AccountBuilder setAccountBalance(int balance) {
        accountBalance = balance;
        return this;
    }

    public AccountBuilder fromAccount(@NonNull Account account) {
        accountId = account.getIdAccount();
        accountName = account.getAccountName();
        accountBalance = account.getAccountBalance();
        return this;
    }

    public Account build() {
        if (isBuilt) {
            throw new IllegalStateException("The builder has already been used. To build another Account create a new builder.");
        } else {
            isBuilt = true;
            if (accountId == lastAccountId + 1) {
                lastAccountId++;
            }
            return new Account(accountId, accountName, accountBalance);
        }
    }

    public static void setLastAccountId (long lastAccountId) {
        AccountBuilder.lastAccountId = lastAccountId;
    }

}
