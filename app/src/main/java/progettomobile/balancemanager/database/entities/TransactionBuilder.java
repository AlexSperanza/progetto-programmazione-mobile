package progettomobile.balancemanager.database.entities;

import org.joda.time.LocalDate;

public class TransactionBuilder {
    private static long lastTransactionIdOfCurrentAccount;

    private long accountId;
    private long transactionId;
    private int value;
    private String description;
    private long categoryId;
    private LocalDate date;
    private long targetAccountId;
    private long targetTransactionId;

    private boolean isBuilt;

    public TransactionBuilder setAccountId(long accountId) {
        this.accountId = accountId;
        return this;
    }

    public TransactionBuilder setTransactionId (long transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    public TransactionBuilder useFirstFreeIdOfCurrentAccount() {
        this.transactionId = lastTransactionIdOfCurrentAccount + 1;
        return this;
    }

    public TransactionBuilder setValue(int value) {
        this.value = value;
        return this;
    }

    public TransactionBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public TransactionBuilder setCategoryId (long categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public TransactionBuilder setDate (LocalDate date) {
        this.date = date;
        return this;
    }

    public TransactionBuilder useToday() {
        this.date = LocalDate.now();
        return this;
    }

    public TransactionBuilder setTargetAccountId(long targetAccountId) {
        this.targetAccountId = targetAccountId;
        return this;
    }

    public TransactionBuilder setTargetTransactionId(long targetTransactionId) {
        this.targetTransactionId = targetTransactionId;
        return this;
    }

    public Transaction build() {
        if (isBuilt) {
            throw new IllegalStateException("This builder has already used. Create another builder to create other transactions.");
        } else {
            isBuilt = true;
            if (transactionId == lastTransactionIdOfCurrentAccount + 1) {
                lastTransactionIdOfCurrentAccount++;
            }
            return new Transaction(transactionId, accountId, date, value, description, categoryId, targetAccountId, targetTransactionId);
        }
    }
}
