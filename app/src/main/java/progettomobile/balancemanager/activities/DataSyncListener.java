package progettomobile.balancemanager.activities;

public interface DataSyncListener {

    void onDataSynced(Runnable callback);
}
