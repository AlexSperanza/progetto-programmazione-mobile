package progettomobile.balancemanager.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.constants.IntentKeys;
import progettomobile.balancemanager.constants.ManagedEntity;
import progettomobile.balancemanager.constants.SharedPreferencesKeys;
import progettomobile.balancemanager.database.Database;
import progettomobile.balancemanager.fragments.AccountSpecificSettingsFragment;
import progettomobile.balancemanager.fragments.GeneralSettingsFragment;
import progettomobile.balancemanager.fragments.InsertAccountFragment;
import progettomobile.balancemanager.fragments.InsertCategoryFragment;
import progettomobile.balancemanager.fragments.InsertTransactionFragment;
import progettomobile.balancemanager.fragments.InsertTransferFragment;
import progettomobile.balancemanager.fragments.LateralMenuFragment;
import progettomobile.balancemanager.utils.FragmentManagementUtils;

import org.joda.time.LocalDate;

import java.util.Objects;

public class BackableActivity extends AppCompatActivity {

    private boolean isFirstStart;

    @Override
    public void onBackPressed() {
        if (isFirstStart) {
            cancelResult();
        } else {
            super.onBackPressed();
        }

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleThemeSetting();
        setContentView(R.layout.backable_activity);

        Intent arrivingIntent = getIntent();
        if (arrivingIntent != null) {
            isFirstStart = arrivingIntent.getBooleanExtra(IntentKeys.FIRST_START, false);
            if (isFirstStart) {
                findViewById(R.id.back_icon_imageView).setVisibility(View.GONE);
                findViewById(R.id.delete_icon_imageView).setVisibility(View.GONE);
            } else {
                findViewById(R.id.back_icon_imageView).setOnClickListener(v -> BackableActivity.this.finish());
            }
            handleBackableType(arrivingIntent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        int styleId = getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, MODE_PRIVATE).getInt(SharedPreferencesKeys.THEME_PREFERENCE, R.style.LightAppTheme);
        setTheme(styleId);
    }

    private void handleBackableType(@NonNull Intent arrivingIntent) {
        ManagedEntity entityType = (ManagedEntity) arrivingIntent.getSerializableExtra(IntentKeys.MANAGED_ENTITY);
        if(entityType == null) {
            cancelResult();
            return;
        }
        switch (entityType) {
            case ACCOUNT:
                long accountId = arrivingIntent.getLongExtra(IntentKeys.EDIT_ACCOUNT_ID, IntentKeys.NO_ACCOUNT_ID);
                InsertAccountFragment insertAccountFragment = new InsertAccountFragment(this, Database.findAccount(accountId));
                FragmentManagementUtils.replaceFragment(this, insertAccountFragment, InsertAccountFragment.class.getName());
                ((TextView) findViewById(R.id.back_activity_textView)).setText(getString(accountId == IntentKeys.NO_ACCOUNT_ID ? R.string.new_account : R.string.edit_account));
                findViewById(R.id.delete_icon_imageView).setVisibility(accountId == IntentKeys.NO_ACCOUNT_ID || Database.getAccountList().size() < 2 ? View.GONE : View.VISIBLE);
                findViewById(R.id.delete_icon_imageView).setOnClickListener(insertAccountFragment.getDeleteButtonListener());
                break;
            case CATEGORY:
                long categoryId = arrivingIntent.getLongExtra(IntentKeys.EDIT_CATEGORY_ID, IntentKeys.NO_CATEGORY_ID);
                InsertCategoryFragment insertCategoryFragment = new InsertCategoryFragment(Database.findCategory(categoryId));
                FragmentManagementUtils.replaceFragment(this, insertCategoryFragment, InsertCategoryFragment.class.getName());
                ((TextView)findViewById(R.id.back_activity_textView)).setText(categoryId == IntentKeys.NO_CATEGORY_ID ? R.string.new_category_title : R.string.edit_category_title);
                findViewById(R.id.delete_icon_imageView).setVisibility(categoryId == IntentKeys.NO_CATEGORY_ID ? View.GONE : View.VISIBLE);
                findViewById(R.id.delete_icon_imageView).setOnClickListener(insertCategoryFragment.getDeleteButtonListener());
                break;
            case TRANSACTION:
                InsertTransactionFragment transactionFragment;
                long transactionId = arrivingIntent.getLongExtra(IntentKeys.EDIT_TRANSACTION_ID, IntentKeys.NO_TRANSACTION_ID);
                String dateString = arrivingIntent.getStringExtra(IntentKeys.TRANSACTION_DATE);
                if (transactionId != IntentKeys.NO_TRANSACTION_ID && dateString != null) {
                    transactionFragment = new InsertTransactionFragment(this, Database.findTransaction(transactionId, LocalDate.parse(dateString)));
                } else if (dateString != null) {
                    transactionFragment = new InsertTransactionFragment(this, LocalDate.parse(dateString));
                } else { // This branch is to prevent error in case of a null date string
                    transactionFragment = new InsertTransactionFragment(this, LocalDate.now());
                }
                FragmentManagementUtils.replaceFragment(this, transactionFragment, InsertTransactionFragment.class.getName());
                ((TextView)findViewById(R.id.back_activity_textView)).setText(transactionId == IntentKeys.NO_TRANSACTION_ID ? R.string.insert_transaction_title : R.string.edit_transaction_title);
                findViewById(R.id.delete_icon_imageView).setVisibility(transactionId == IntentKeys.NO_TRANSACTION_ID ? View.GONE : View.VISIBLE);
                findViewById(R.id.delete_icon_imageView).setOnClickListener(transactionFragment.getDeleteButtonListener());
                break;
            case TRANSFER:
                InsertTransferFragment transferFragment;
                long transferId = arrivingIntent.getLongExtra(IntentKeys.EDIT_TRANSACTION_ID, IntentKeys.NO_TRANSACTION_ID);
                String transferDateString = arrivingIntent.getStringExtra(IntentKeys.TRANSACTION_DATE);
                if (transferId != IntentKeys.NO_TRANSACTION_ID && transferDateString != null) {
                    transferFragment = new InsertTransferFragment(this, Database.findTransaction(transferId, LocalDate.parse(transferDateString)));
                } else if (transferDateString != null) {
                    transferFragment = new InsertTransferFragment(this, LocalDate.parse(transferDateString));
                } else {
                    transferFragment = new InsertTransferFragment(this, LocalDate.now());
                }
                FragmentManagementUtils.replaceFragment(this, transferFragment, InsertTransferFragment.class.getName());
                ((TextView)findViewById(R.id.back_activity_textView)).setText(transferId == IntentKeys.NO_TRANSACTION_ID ? R.string.insert_transfer_title : R.string.edit_transfer_title);
                findViewById(R.id.delete_icon_imageView).setVisibility(transferId == IntentKeys.NO_TRANSACTION_ID ? View.GONE : View.VISIBLE);
                findViewById(R.id.delete_icon_imageView).setOnClickListener(transferFragment.getDeleteButtonListener());
                break;
            case MENU_LIST:
                FragmentManagementUtils.replaceFragment(this, new LateralMenuFragment(), LateralMenuFragment.class.getName());
                ((TextView)findViewById(R.id.back_activity_textView)).setText(R.string.navigation_menu);
                findViewById(R.id.delete_icon_imageView).setVisibility(View.GONE);
                break;
            case SETTING_PAGE:
                long settingAccountId = arrivingIntent.getLongExtra(IntentKeys.SETTING_ACCOUNT_ID, IntentKeys.GENERAL_SETTINGS);
                if (settingAccountId == IntentKeys.GENERAL_SETTINGS) {
                    FragmentManagementUtils.replaceFragment(this, new GeneralSettingsFragment(this), GeneralSettingsFragment.class.getName());
                    ((TextView)findViewById(R.id.back_activity_textView)).setText(R.string.general_settings);
                    findViewById(R.id.delete_icon_imageView).setVisibility(View.GONE);
                } else {
                    String accountName = Objects.requireNonNull(Database.findAccount(settingAccountId)).getAccountName();
                    FragmentManagementUtils.replaceFragment(this, new AccountSpecificSettingsFragment(this, settingAccountId), GeneralSettingsFragment.class.getName());
                    ((TextView)findViewById(R.id.back_activity_textView)).setText(getString(R.string.account_settings_title, accountName));
                    findViewById(R.id.delete_icon_imageView).setVisibility(View.GONE);
                }

                break;
        }
    }

    private void cancelResult() {
        setResult(RESULT_CANCELED, null);
        this.finish();
    }

    private void handleThemeSetting() {
        int styleId = getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, MODE_PRIVATE).getInt(SharedPreferencesKeys.THEME_PREFERENCE, R.style.LightAppTheme);
        setTheme(styleId);
    }
}
