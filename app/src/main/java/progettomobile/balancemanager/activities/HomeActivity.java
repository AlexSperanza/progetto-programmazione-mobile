package progettomobile.balancemanager.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.kloadingspin.KLoadingSpin;
import progettomobile.balancemanager.R;
import progettomobile.balancemanager.constants.ApplicationPage;
import progettomobile.balancemanager.constants.IntentKeys;
import progettomobile.balancemanager.constants.ManagedEntity;
import progettomobile.balancemanager.constants.RequestCodes;
import progettomobile.balancemanager.constants.SharedPreferencesKeys;
import progettomobile.balancemanager.database.Database;
import progettomobile.balancemanager.database.entities.Account;
import progettomobile.balancemanager.fragments.AccountListFragment;
import progettomobile.balancemanager.fragments.CategoryListFragment;
import progettomobile.balancemanager.fragments.CurrentAccountFragment;

public class HomeActivity extends AppCompatActivity implements DataSyncListener {
    private boolean isDataSync;

    @Override
    public void onDataSynced(Runnable callback) {
        isDataSync = true;
        hideDialog();
        Database.removeObserver(this);
        callback.run();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleThemeSetting();
        setContentView(R.layout.activity_home);
        isDataSync = false;
        if (Database.getDbInstance() == null) { //Ensures the database is initialized
            Database.initAppDatabase(getApplication());
        }
        setupView();
        Intent intent = getIntent();
        if (intent == null
            || (intent.getAction() != null && intent.getAction().equals(Intent.ACTION_MAIN))) {
            Database.registerObserver(this, this::loadLastAccount);
            handleStart();
        } else {
            handleIntent(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            handleIntent(data);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Database.removeObserver(this);
    }

    private void onFirstStart() {
        Intent intent = new Intent(this, BackableActivity.class);
        intent.putExtra(IntentKeys.FIRST_START, true);
        intent.putExtra(IntentKeys.MANAGED_ENTITY, ManagedEntity.ACCOUNT);
        startActivityForResult(intent, RequestCodes.REQUEST_NEW_ACCOUNT);

        Intent tutorialIntent = new Intent(this, SimpleActivity.class);
        tutorialIntent.putExtra(IntentKeys.FIRST_START, true);
        tutorialIntent.putExtra(IntentKeys.PAGE, ApplicationPage.TUTORIAL);
        startActivity(tutorialIntent);
    }

    private void loadLastAccount() {
        Account lastAccount = Database.findAccount(getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE)
                .getLong(SharedPreferencesKeys.LAST_ACCOUNT, 0));
        if (lastAccount != null) {
            ((TextView) findViewById(R.id.home_title_textView)).setText(lastAccount.getAccountName());
        }
        CurrentAccountFragment currentAccountFragment = new CurrentAccountFragment(this);
        findViewById(R.id.add_icon_imageview).setOnClickListener(currentAccountFragment.getAddButtonListener());
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.current_account_fragment_container, currentAccountFragment)
                .commit();
    }

    private boolean isFirstStart() {
        return this.getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, Context.MODE_PRIVATE)
                   .getLong(SharedPreferencesKeys.LAST_ACCOUNT, 0) == 0;
    }

    private void handleStart() {
        if (isFirstStart()) {
            onFirstStart();
        } else {
            showDialog();
            new Handler().post(Database::syncData);
            //new Handler().postDelayed(Database::syncData, 2000);
        }
    }

    private void setupView() {
        findViewById(R.id.menu_icon_imageview).setOnClickListener(v -> {
            Intent intent = new Intent(HomeActivity.this, BackableActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(IntentKeys.MANAGED_ENTITY, ManagedEntity.MENU_LIST);
            startActivity(intent);
        });
    }

    private void handleIntent(@NonNull Intent data) {
        ApplicationPage page = (ApplicationPage) data.getSerializableExtra(IntentKeys.PAGE);
        if (page == null) {
            page = ApplicationPage.LAST_ACCOUNT;
        }
        switch (page) {
            case ACCOUNT_LIST:
                Database.registerObserver(this, this::loadAccountList);
                showDialog();
                new Handler().post(Database::syncData);
                break;
            case LAST_ACCOUNT:
                Database.registerObserver(this, this::loadLastAccount);
                handleStart();
                break;
            case CATEGORY_LIST:
                loadCategoryList();
                break;
            default:
                break;
        }

    }

    private void showDialog() {
        if (!isDataSync) {
            KLoadingSpin dialog = findViewById(R.id.home_loading_dialog);
            dialog.startAnimation();
            dialog.setIsVisible(true);
            dialog.setTranslationZ(1);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    private void hideDialog() {
        KLoadingSpin dialog = findViewById(R.id.home_loading_dialog);
        dialog.setIsVisible(false);
        dialog.invalidate();
        dialog.setTranslationZ(-1);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void handleThemeSetting() {
        int styleId = getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, MODE_PRIVATE).getInt(SharedPreferencesKeys.THEME_PREFERENCE, R.style.LightAppTheme);
        setTheme(styleId);
    }

    private void loadAccountList() {
        AccountListFragment accountListFragment = new AccountListFragment(this);
        ((TextView)findViewById(R.id.home_title_textView)).setText(getString(R.string.page_account_list));
        findViewById(R.id.add_icon_imageview).setOnClickListener(accountListFragment.getAddButtonListener());
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.current_account_fragment_container, accountListFragment)
                .commit();
    }

    private void loadCategoryList() {
        CategoryListFragment categoryListFragment = new CategoryListFragment(this);
        ((TextView)findViewById(R.id.home_title_textView)).setText(getString(R.string.page_category_list));
        findViewById(R.id.add_icon_imageview).setOnClickListener(categoryListFragment.getAddButtonListener());
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.current_account_fragment_container, categoryListFragment)
                .commit();
    }
}
