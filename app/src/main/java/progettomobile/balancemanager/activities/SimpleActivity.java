package progettomobile.balancemanager.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import progettomobile.balancemanager.R;
import progettomobile.balancemanager.constants.ApplicationPage;
import progettomobile.balancemanager.constants.IntentKeys;
import progettomobile.balancemanager.constants.ManagedEntity;
import progettomobile.balancemanager.constants.SharedPreferencesKeys;
import progettomobile.balancemanager.fragments.CreditFragment;
import progettomobile.balancemanager.fragments.SettingSectionListFragment;
import progettomobile.balancemanager.fragments.TutorialFragment;

public class SimpleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleThemeSetting();
        setContentView(R.layout.simple_activity);

        setupView();

        Intent intent = getIntent();
        if (intent != null) {
            handleIntent(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        int styleId = getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, MODE_PRIVATE).getInt(SharedPreferencesKeys.THEME_PREFERENCE, R.style.LightAppTheme);
        setTheme(styleId);
        Intent intent = getIntent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        finish();
    }

    private void setupView() {
        findViewById(R.id.menu_icon_imageview).setOnClickListener(v -> {
            Intent intent = new Intent(SimpleActivity.this, BackableActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(IntentKeys.MANAGED_ENTITY, ManagedEntity.MENU_LIST);
            startActivity(intent);
        });
    }

    private void handleIntent (@NonNull Intent intent) {
        ApplicationPage page = (ApplicationPage) intent.getSerializableExtra(IntentKeys.PAGE);
        if (page == null) {
            page = ApplicationPage.SETTINGS;
        }
        switch (page) {
            case SETTINGS:
                ((TextView)findViewById(R.id.simple_activity_title_textView)).setText(R.string.simple_activity_setting_title);
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.simple_activity_fragment_container, new SettingSectionListFragment(this))
                        .commit();
                break;
            case CREDITS:
                ((TextView)findViewById(R.id.simple_activity_title_textView)).setText(R.string.page_credits);
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.simple_activity_fragment_container, new CreditFragment())
                        .commit();
                break;
            case TUTORIAL:
                ((TextView)findViewById(R.id.simple_activity_title_textView)).setText(R.string.page_tutorial);
                boolean isFirstStart = intent.getBooleanExtra(IntentKeys.FIRST_START, false);
                findViewById(R.id.menu_icon_imageview).setVisibility(isFirstStart ? View.GONE : View.VISIBLE);
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.simple_activity_fragment_container, new TutorialFragment(this, isFirstStart))
                        .commit();
                break;
            default:
                break;
        }

    }

    private void handleThemeSetting() {
        int styleId = getSharedPreferences(SharedPreferencesKeys.GENERAL_PREFERENCES, MODE_PRIVATE).getInt(SharedPreferencesKeys.THEME_PREFERENCE, R.style.LightAppTheme);
        setTheme(styleId);
    }
}
